<?php
// Heading
$_['heading_title']     = 'Reviews';

// Text
$_['text_success']      = 'Success: You have modified reviews!';
$_['text_list']         = 'Review List';
$_['text_add']          = 'Add Review';
$_['text_edit']         = 'Edit Review';
$_['text_filter']       = 'Filter';

// Column
$_['column_product']    = 'Product';
$_['column_author']     = 'Author';
$_['column_rating']     = 'Rating';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';


$_['column_information']       = 'Article';

// Button
$_['button_type_review']       = 'Type Categories';

// Entry
$_['entry_reply']              = 'Reply';
$_['entry_review_information'] = 'Articles';
$_['entry_review_product']     = 'Products';
$_['entry_information']        = 'Article';
        
$_['entry_product']     = 'Product';
$_['entry_author']      = 'Author';
$_['entry_rating']      = 'Rating';
$_['entry_status']      = 'Status';
$_['entry_text']        = 'Text';
$_['entry_date_added']  = 'Date Added';

// Help
$_['help_product']      = '(Autocomplete)';


$_['help_information']         = '(Autocomplete)';

// Error
$_['error_information']        = 'Article required!';
        
$_['error_permission']  = 'Warning: You do not have permission to modify reviews!';
$_['error_product']     = 'Product required!';
$_['error_author']      = 'Author must be between 3 and 64 characters!';
$_['error_text']        = 'Review Text must be at least 1 character!';
$_['error_rating']      = 'Review rating required!';