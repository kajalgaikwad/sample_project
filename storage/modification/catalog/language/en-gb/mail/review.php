<?php
// Text

$_['text_information_subject'] = '%s - Review Article';
$_['text_information']         = 'Article: %s';
        
$_['text_subject']  = '%s - Product Review';
$_['text_waiting']  = 'You have a new product review waiting.';
$_['text_product']  = 'Product: %s';
$_['text_reviewer'] = 'Reviewer: %s';
$_['text_rating']   = 'Rating: %s';
$_['text_review']   = 'Review Text:';