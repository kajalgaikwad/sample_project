<?php

/* catalog/product_list.twig */
class __TwigTemplate_f4a24ca9a07647bdf58343407db651ee80325d7aad881c0264f580f54f90dbff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" onclick=\"\$('#filter-product').toggleClass('hidden-sm hidden-xs');\" class=\"btn btn-default hidden-md hidden-lg\"><i class=\"fa fa-filter\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
        <button type=\"submit\" form=\"form-product\" formaction=\"";
        // line 8
        echo (isset($context["copy"]) ? $context["copy"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_copy"]) ? $context["button_copy"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-copy\"></i></button>
        <button type=\"button\" form=\"form-product\" formaction=\"";
        // line 9
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-product').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 11
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 20
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 24
        echo "    ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 25
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 29
        echo "    <div class=\"row\">
      <div id=\"filter-product\" class=\"col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs\">
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h3 class=\"panel-title\"><i class=\"fa fa-filter\"></i> ";
        // line 33
        echo (isset($context["text_filter"]) ? $context["text_filter"] : null);
        echo "</h3>
          </div>
          <div class=\"panel-body\">
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-name\">";
        // line 37
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
              <input type=\"text\" name=\"filter_name\" value=\"";
        // line 38
        echo (isset($context["filter_name"]) ? $context["filter_name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-model\">";
        // line 41
        echo (isset($context["entry_model"]) ? $context["entry_model"] : null);
        echo "</label>
              <input type=\"text\" name=\"filter_model\" value=\"";
        // line 42
        echo (isset($context["filter_model"]) ? $context["filter_model"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_model"]) ? $context["entry_model"] : null);
        echo "\" id=\"input-model\" class=\"form-control\" />
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-price\">";
        // line 45
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "</label>
              <input type=\"text\" name=\"filter_price\" value=\"";
        // line 46
        echo (isset($context["filter_price"]) ? $context["filter_price"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_price"]) ? $context["entry_price"] : null);
        echo "\" id=\"input-price\" class=\"form-control\" />
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-quantity\">";
        // line 49
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "</label>
              <input type=\"text\" name=\"filter_quantity\" value=\"";
        // line 50
        echo (isset($context["filter_quantity"]) ? $context["filter_quantity"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_quantity"]) ? $context["entry_quantity"] : null);
        echo "\" id=\"input-quantity\" class=\"form-control\" />
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-status\">";
        // line 53
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
              <select name=\"filter_status\" id=\"input-status\" class=\"form-control\">
                <option value=\"\"></option>
                
                
                
                  
                

                  ";
        // line 62
        if (((isset($context["filter_status"]) ? $context["filter_status"] : null) == "1")) {
            // line 63
            echo "
                
                
                  
                
                
                <option value=\"1\" selected=\"selected\">";
            // line 69
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                
                
                
                  
                

                  ";
        } else {
            // line 77
            echo "
                
                
                  
                
                
                <option value=\"1\">";
            // line 83
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                
                
                
                  
                

                  ";
        }
        // line 91
        echo "                  ";
        if (((isset($context["filter_status"]) ? $context["filter_status"] : null) == "0")) {
            // line 92
            echo "
                
                
                  
                
                
                <option value=\"0\" selected=\"selected\">";
            // line 98
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                
                
                
                  
                

                  ";
        } else {
            // line 106
            echo "
                
                
                  
                
                
                <option value=\"0\">";
            // line 112
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                
                
                
                  
                

                  ";
        }
        // line 120
        echo "

              
              
                
              
              
              </select>
            </div>
            <div class=\"form-group text-right\">
              <button type=\"button\" id=\"button-filter\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 130
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
      <div class=\"col-md-9 col-md-pull-3 col-sm-12\">
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 138
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
          </div>
          <div class=\"panel-body\">
            <form action=\"";
        // line 141
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-product\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                      <td class=\"text-center\">";
        // line 147
        echo (isset($context["column_image"]) ? $context["column_image"] : null);
        echo "</td>
                      <td class=\"text-left\">";
        // line 148
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
            echo " <a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-left\">";
        // line 149
        if (((isset($context["sort"]) ? $context["sort"] : null) == "p.model")) {
            echo " <a href=\"";
            echo (isset($context["sort_model"]) ? $context["sort_model"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_model"]) ? $context["column_model"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_model"]) ? $context["sort_model"] : null);
            echo "\">";
            echo (isset($context["column_model"]) ? $context["column_model"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-right\">";
        // line 150
        if (((isset($context["sort"]) ? $context["sort"] : null) == "p.price")) {
            echo " <a href=\"";
            echo (isset($context["sort_price"]) ? $context["sort_price"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_price"]) ? $context["column_price"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_price"]) ? $context["sort_price"] : null);
            echo "\">";
            echo (isset($context["column_price"]) ? $context["column_price"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-right\">";
        // line 151
        if (((isset($context["sort"]) ? $context["sort"] : null) == "p.quantity")) {
            echo " <a href=\"";
            echo (isset($context["sort_quantity"]) ? $context["sort_quantity"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_quantity"]) ? $context["sort_quantity"] : null);
            echo "\">";
            echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-left\">";
        // line 152
        if (((isset($context["sort"]) ? $context["sort"] : null) == "p.status")) {
            echo " <a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\">";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-right\">";
        // line 153
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                  
                  ";
        // line 158
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 159
            echo "                  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 160
                echo "                  <tr>
                    <td class=\"text-center\">";
                // line 161
                if (twig_in_filter($this->getAttribute($context["product"], "product_id", array()), (isset($context["selected"]) ? $context["selected"] : null))) {
                    // line 162
                    echo "                      <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" checked=\"checked\" />
                      ";
                } else {
                    // line 164
                    echo "                      <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" />
                      ";
                }
                // line 165
                echo "</td>
                    <td class=\"text-center\">";
                // line 166
                if ($this->getAttribute($context["product"], "image", array())) {
                    echo " <img src=\"";
                    echo $this->getAttribute($context["product"], "image", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-thumbnail\" /> ";
                } else {
                    echo " <span class=\"img-thumbnail list\"><i class=\"fa fa-camera fa-2x\"></i></span> ";
                }
                echo "</td>
                    
\t\t\t<td class=\"text-left\">";
                // line 168
                echo $this->getAttribute($context["product"], "name", array());
                echo " <a href=\"";
                echo $this->getAttribute($context["product"], "front_link", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_view"]) ? $context["button_view"] : null);
                echo "\" TARGET=\"_BLANK\"><i class=\"fa fa-external-link\"></i></a></td>
\t\t\t\t
                    <td class=\"text-left\">";
                // line 170
                echo $this->getAttribute($context["product"], "model", array());
                echo "</td>
                    <td class=\"text-right\">";
                // line 171
                if ($this->getAttribute($context["product"], "special", array())) {
                    echo " <span style=\"text-decoration: line-through;\">";
                    echo $this->getAttribute($context["product"], "price", array());
                    echo "</span><br/>
                      <div class=\"text-danger\">";
                    // line 172
                    echo $this->getAttribute($context["product"], "special", array());
                    echo "</div>
                      ";
                } else {
                    // line 174
                    echo "                      
\t\t\t\t<div id=\"show_price_input_";
                    // line 175
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" onclick=\"show_input('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "')\">";
                    echo $this->getAttribute($context["product"], "raw_price", array());
                    echo "</div>
\t\t\t\t<input type=\"text\" value=\"";
                    // line 176
                    echo $this->getAttribute($context["product"], "raw_price", array());
                    echo "\" placeholder=\"price\" id=\"price_";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" class=\"form-control input-sm\" style=\"display:none;\" />
\t\t\t\t<div id=\"button_price_";
                    // line 177
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\">
\t\t\t\t\t<span class=\"btn btn-primary btn-xs edit\" onclick=\"edit_price_input('";
                    // line 178
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "')\"><i class=\"fa fa-pencil\"></i></span>
\t\t\t\t\t<span class=\"btn btn-primary btn-xs save\" style=\"display:none\" onclick=\"save_price_input('";
                    // line 179
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "')\"><i class=\"fa fa-check\"></i></span>
\t\t\t\t\t<span class=\"btn btn-default btn-xs cancel\" style=\"display:none\" onclick=\"cancel_price_input('";
                    // line 180
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "')\"><i class=\"fa fa-times\"></i></span>
\t\t\t\t</div>
\t\t\t\t
                      ";
                }
                // line 183
                echo "</td>
                    
\t\t\t<td class=\"text-right\">
\t\t\t\t<div id=\"show_quantity_input_";
                // line 186
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\">
\t\t\t\t\t";
                // line 187
                if (($this->getAttribute($context["product"], "quantity", array()) <= 0)) {
                    echo " <span class=\"label label-warning\">";
                    echo $this->getAttribute($context["product"], "quantity", array());
                    echo "</span> ";
                } elseif (($this->getAttribute($context["product"], "quantity", array()) <= 5)) {
                    echo " <span class=\"label label-danger\">";
                    echo $this->getAttribute($context["product"], "quantity", array());
                    echo "</span> ";
                } else {
                    echo " <span class=\"label label-success\">";
                    echo $this->getAttribute($context["product"], "quantity", array());
                    echo "</span> ";
                }
                echo " 
\t\t\t\t</div>
\t\t\t\t<input type=\"number\" value=\"";
                // line 189
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "\" placeholder=\"quantity\" id=\"quantity_";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\" class=\"form-control input-sm\" style=\"display:none;width:90px;\" />
\t\t\t\t<div id=\"button_quantity_";
                // line 190
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\">
\t\t\t\t\t<span class=\"btn btn-primary btn-xs edit\" onclick=\"edit_quantity_input('";
                // line 191
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "')\"><i class=\"fa fa-pencil\"></i></span>
\t\t\t\t\t<span class=\"btn btn-primary btn-xs save\" style=\"display:none\" onclick=\"save_quantity_input('";
                // line 192
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "')\"><i class=\"fa fa-check\"></i></span>
\t\t\t\t\t<span class=\"btn btn-default btn-xs cancel\" style=\"display:none\" onclick=\"cancel_quantity_input('";
                // line 193
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "')\"><i class=\"fa fa-times\"></i></span>
\t\t\t\t</div>
\t\t\t</td>
\t\t\t\t
                    
\t\t\t<td class=\"text-left\">
\t\t\t\t<div id=\"status_";
                // line 199
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\">
\t\t\t\t\t<div>
\t\t\t\t\t";
                // line 201
                if (($this->getAttribute($context["product"], "status", array()) == 1)) {
                    // line 202
                    echo " \t\t\t\t\t\t ";
                    echo "<div class='enabled'><i class='fa fa-check'></i> <b>Enabled</b></div>";
                    echo "
\t\t\t\t\t";
                } else {
                    // line 204
                    echo "\t\t\t\t\t\t ";
                    echo "<div class='disabled'><i class='fa fa-times'></i> <b>Disabled</b></div>";
                    echo "
\t\t\t\t\t";
                }
                // line 206
                echo "\t\t\t\t\t</div>  
\t\t\t\t\t  ";
                // line 207
                if (($this->getAttribute($context["product"], "status", array()) == 1)) {
                    // line 208
                    echo "\t\t\t\t\t  <a class=\"status_";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo " small\" onclick=\"change_status('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "','0')\" data-toggle=\"tooltip\" title=\"Click to  ";
                    echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
                    echo " \" ><i class='fa fa-times'></i> ";
                    echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
                    echo " </a>
\t\t\t\t\t ";
                } else {
                    // line 210
                    echo "\t\t\t\t\t  <a class=\"status_";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo " small\" onclick=\"change_status('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "','1')\" data-toggle=\"tooltip\" title=\"Click to  ";
                    echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
                    echo " \" ><i class='fa fa-check'></i>  ";
                    echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
                    echo "</a>
\t\t\t\t\t ";
                }
                // line 212
                echo "\t\t\t\t</div>
\t\t\t</td>
\t\t\t\t
                    <td class=\"text-right\"><a href=\"";
                // line 215
                echo $this->getAttribute($context["product"], "edit", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                  </tr>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 218
            echo "                  ";
        } else {
            // line 219
            echo "                  <tr>
                    <td class=\"text-center\" colspan=\"8\">";
            // line 220
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
                  </tr>
                  ";
        }
        // line 223
        echo "                    </tbody>
                  
                </table>
              </div>
            </form>
            <div class=\"row\">
              <div class=\"col-sm-6 text-left\">";
        // line 229
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
              <div class=\"col-sm-6 text-right\">";
        // line 230
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\tvar url = '';

\tvar filter_name = \$('input[name=\\'filter_name\\']').val();

\tif (filter_name) {
\t\turl += '&filter_name=' + encodeURIComponent(filter_name);
\t}

\tvar filter_model = \$('input[name=\\'filter_model\\']').val();

\tif (filter_model) {
\t\turl += '&filter_model=' + encodeURIComponent(filter_model);
\t}

\tvar filter_price = \$('input[name=\\'filter_price\\']').val();

\tif (filter_price) {
\t\turl += '&filter_price=' + encodeURIComponent(filter_price);
\t}

\tvar filter_quantity = \$('input[name=\\'filter_quantity\\']').val();

\tif (filter_quantity) {
\t\turl += '&filter_quantity=' + encodeURIComponent(filter_quantity);
\t}

\tvar filter_status = \$('select[name=\\'filter_status\\']').val();

\tif (filter_status !== '') {
\t\turl += '&filter_status=' + encodeURIComponent(filter_status);
\t}

\tlocation = 'index.php?route=catalog/product&user_token=";
        // line 271
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "' + url;
});
//--></script> 
  <script type=\"text/javascript\"><!--
// IE and Edge fix!
\$('button[form=\\'form-product\\']').on('click', function(e) {
\t\$('#form-product').attr('action', \$(this).attr('formaction'));
});
  
\$('input[name=\\'filter_name\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 283
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_name\\']').val(item['label']);
\t}
});

\$('input[name=\\'filter_model\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 303
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_model=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['model'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_model\\']').val(item['label']);
\t}
});
//--></script></div>

\t\t\t\t  <script type=\"text/javascript\">
\t\t\t\t\tfunction change_status(id,statusid){
\t\t\t\t\t\t\$('#status_'+id).addClass(\"loader16\");
\t\t\t\t\t\t\$('.status_'+id).remove();
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\turl: 'index.php?route=catalog/product/changestatusid&user_token=";
        // line 326
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&product_id='+id+'&status='+statusid,
\t\t\t\t\t\t\tsuccess: function(data) {
\t\t\t\t\t\t\t\$('#status_'+id).html(data);
\t\t\t\t\t\t\t\$('#status_'+id).removeClass(\"loader16\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t  });
\t\t\t\t\t}
\t\t\t\t\tfunction save_price_input(id){
\t\t\t\t\t\t\$('#price_'+id).prop(\"disabled\",true);
\t\t\t\t\t\tvar price = \$('#price_'+id).val();
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\turl: 'index.php?route=catalog/product/changeproductprice&user_token=";
        // line 337
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&product_id='+id+'&price='+price,
\t\t\t\t\t\t\tcomplete: function () {
\t\t\t\t\t\t\t\t\$('#button_price_'+id+' .save').hide();
\t\t\t\t\t\t\t\t\$('#button_price_'+id+' .cancel').hide();
\t\t\t\t\t\t\t\t\$('#button_price_'+id+' .edit').show();
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tsuccess: function(data) {
\t\t\t\t\t\t\t\$('#price_'+id).prop(\"disabled\",false);
\t\t\t\t\t\t\t\$('#show_price_input_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\t\$('#show_price_input_'+id).empty();
\t\t\t\t\t\t\t\$('#show_price_input_'+id).append(data);
\t\t\t\t\t\t\t\$('#price_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t  });
\t\t\t\t\t}
\t\t\t\t\tfunction edit_price_input(id){
\t\t\t\t\t\t\$('#price_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\$('#show_price_input_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\$('#button_price_'+ id +' .save').show();
\t\t\t\t\t\t\$('#button_price_'+ id +' .cancel').show();
\t\t\t\t\t\t\$('#button_price_'+ id +' .edit').hide();
\t\t\t\t\t}
\t\t\t\t\tfunction cancel_price_input(id){
\t\t\t\t\t\t\$('#price_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\$('#show_price_input_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\$('#button_price_'+id+' .save').hide();
\t\t\t\t\t\t\$('#button_price_'+id+' .cancel').hide();
\t\t\t\t\t\t\$('#button_price_'+id+' .edit').show();
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\tfunction save_quantity_input(id){
\t\t\t\t\t\t\$('#quantity_'+id).prop(\"disabled\",true);
\t\t\t\t\t\tvar quantity = \$('#quantity_'+id).val();
\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\turl: 'index.php?route=catalog/product/changeproductquantity&user_token=";
        // line 371
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&product_id='+id+'&quantity='+quantity,
\t\t\t\t\t\t\tcomplete: function () {
\t\t\t\t\t\t\t\t\$('#button_quantity_'+id+' .save').hide();
\t\t\t\t\t\t\t\t\$('#button_quantity_'+id+' .cancel').hide();
\t\t\t\t\t\t\t\t\$('#button_quantity_'+id+' .edit').show();
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tsuccess: function(data) {
\t\t\t\t\t\t\t\$('#quantity_'+id).prop(\"disabled\",false);
\t\t\t\t\t\t\t\$('#show_quantity_input_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\t\$('#show_quantity_input_'+id).empty();
\t\t\t\t\t\t\tif(data <= 0){
\t\t\t\t\t\t\t\t\$('#show_quantity_input_'+id).append('<span class=\"label label-warning\">' + data + '</span>');
\t\t\t\t\t\t\t} else if(data <= 5) {
\t\t\t\t\t\t\t\t\$('#show_quantity_input_'+id).append('<span class=\"label label-danger\">' + data + '</span>');
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\$('#show_quantity_input_'+id).append('<span class=\"label label-success\">' + data + '</span>');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$('#quantity_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t  });
\t\t\t\t\t}
\t\t\t\t\tfunction edit_quantity_input(id){
\t\t\t\t\t\t\$('#quantity_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\$('#show_quantity_input_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\$('#button_quantity_'+ id +' .save').show();
\t\t\t\t\t\t\$('#button_quantity_'+ id +' .cancel').show();
\t\t\t\t\t\t\$('#button_quantity_'+ id +' .edit').hide();
\t\t\t\t\t}
\t\t\t\t\tfunction cancel_quantity_input(id){
\t\t\t\t\t\t\$('#quantity_'+id).css(\"display\",\"none\");
\t\t\t\t\t\t\$('#show_quantity_input_'+id).css(\"display\",\"block\");
\t\t\t\t\t\t\$('#button_quantity_'+id+' .save').hide();
\t\t\t\t\t\t\$('#button_quantity_'+id+' .cancel').hide();
\t\t\t\t\t\t\$('#button_quantity_'+id+' .edit').show();
\t\t\t\t\t}
\t\t\t\t</script>
\t\t\t\t<style>
\t\t\t\t\t.enabled  {
\t\t\t\t\tcolor: #0fa000;
\t\t\t\t}
\t\t\t\t.disabled  {
\t\t\t\t\tcolor: #ff0000;
\t\t\t\t}
\t\t\t\t</style>
";
        // line 415
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "catalog/product_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  840 => 415,  793 => 371,  756 => 337,  742 => 326,  716 => 303,  693 => 283,  678 => 271,  634 => 230,  630 => 229,  622 => 223,  616 => 220,  613 => 219,  610 => 218,  599 => 215,  594 => 212,  582 => 210,  570 => 208,  568 => 207,  565 => 206,  559 => 204,  553 => 202,  551 => 201,  546 => 199,  537 => 193,  533 => 192,  529 => 191,  525 => 190,  519 => 189,  502 => 187,  498 => 186,  493 => 183,  486 => 180,  482 => 179,  478 => 178,  474 => 177,  468 => 176,  460 => 175,  457 => 174,  452 => 172,  446 => 171,  442 => 170,  433 => 168,  420 => 166,  417 => 165,  411 => 164,  405 => 162,  403 => 161,  400 => 160,  395 => 159,  393 => 158,  385 => 153,  367 => 152,  349 => 151,  331 => 150,  313 => 149,  295 => 148,  291 => 147,  282 => 141,  276 => 138,  265 => 130,  253 => 120,  242 => 112,  234 => 106,  223 => 98,  215 => 92,  212 => 91,  201 => 83,  193 => 77,  182 => 69,  174 => 63,  172 => 62,  160 => 53,  152 => 50,  148 => 49,  140 => 46,  136 => 45,  128 => 42,  124 => 41,  116 => 38,  112 => 37,  105 => 33,  99 => 29,  91 => 25,  88 => 24,  80 => 20,  78 => 19,  73 => 16,  62 => 14,  58 => 13,  53 => 11,  44 => 9,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="button" data-toggle="tooltip" title="{{ button_filter }}" onclick="$('#filter-product').toggleClass('hidden-sm hidden-xs');" class="btn btn-default hidden-md hidden-lg"><i class="fa fa-filter"></i></button>*/
/*         <a href="{{ add }}" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/*         <button type="submit" form="form-product" formaction="{{ copy }}" data-toggle="tooltip" title="{{ button_copy }}" class="btn btn-default"><i class="fa fa-copy"></i></button>*/
/*         <button type="button" form="form-product" formaction="{{ delete }}" data-toggle="tooltip" title="{{ button_delete }}" class="btn btn-danger" onclick="confirm('{{ text_confirm }}') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>*/
/*       </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">{% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     {% if success %}*/
/*     <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="row">*/
/*       <div id="filter-product" class="col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs">*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h3 class="panel-title"><i class="fa fa-filter"></i> {{ text_filter }}</h3>*/
/*           </div>*/
/*           <div class="panel-body">*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/*               <input type="text" name="filter_name" value="{{ filter_name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-model">{{ entry_model }}</label>*/
/*               <input type="text" name="filter_model" value="{{ filter_model }}" placeholder="{{ entry_model }}" id="input-model" class="form-control" />*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-price">{{ entry_price }}</label>*/
/*               <input type="text" name="filter_price" value="{{ filter_price }}" placeholder="{{ entry_price }}" id="input-price" class="form-control" />*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-quantity">{{ entry_quantity }}</label>*/
/*               <input type="text" name="filter_quantity" value="{{ filter_quantity }}" placeholder="{{ entry_quantity }}" id="input-quantity" class="form-control" />*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-status">{{ entry_status }}</label>*/
/*               <select name="filter_status" id="input-status" class="form-control">*/
/*                 <option value=""></option>*/
/*                 */
/*                 */
/*                 */
/*                   */
/*                 */
/* */
/*                   {% if filter_status == '1' %}*/
/* */
/*                 */
/*                 */
/*                   */
/*                 */
/*                 */
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 */
/*                 */
/*                 */
/*                   */
/*                 */
/* */
/*                   {% else %}*/
/* */
/*                 */
/*                 */
/*                   */
/*                 */
/*                 */
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 */
/*                 */
/*                 */
/*                   */
/*                 */
/* */
/*                   {% endif %}*/
/*                   {% if filter_status == '0' %}*/
/* */
/*                 */
/*                 */
/*                   */
/*                 */
/*                 */
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 */
/*                 */
/*                 */
/*                   */
/*                 */
/* */
/*                   {% else %}*/
/* */
/*                 */
/*                 */
/*                   */
/*                 */
/*                 */
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 */
/*                 */
/*                 */
/*                   */
/*                 */
/* */
/*                   {% endif %}*/
/* */
/* */
/*               */
/*               */
/*                 */
/*               */
/*               */
/*               </select>*/
/*             </div>*/
/*             <div class="form-group text-right">*/
/*               <button type="button" id="button-filter" class="btn btn-default"><i class="fa fa-filter"></i> {{ button_filter }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="col-md-9 col-md-pull-3 col-sm-12">*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/*           </div>*/
/*           <div class="panel-body">*/
/*             <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-product">*/
/*               <div class="table-responsive">*/
/*                 <table class="table table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/*                       <td class="text-center">{{ column_image }}</td>*/
/*                       <td class="text-left">{% if sort == 'pd.name' %} <a href="{{ sort_name }}" class="{{ order|lower }}">{{ column_name }}</a> {% else %} <a href="{{ sort_name }}">{{ column_name }}</a> {% endif %}</td>*/
/*                       <td class="text-left">{% if sort == 'p.model' %} <a href="{{ sort_model }}" class="{{ order|lower }}">{{ column_model }}</a> {% else %} <a href="{{ sort_model }}">{{ column_model }}</a> {% endif %}</td>*/
/*                       <td class="text-right">{% if sort == 'p.price' %} <a href="{{ sort_price }}" class="{{ order|lower }}">{{ column_price }}</a> {% else %} <a href="{{ sort_price }}">{{ column_price }}</a> {% endif %}</td>*/
/*                       <td class="text-right">{% if sort == 'p.quantity' %} <a href="{{ sort_quantity }}" class="{{ order|lower }}">{{ column_quantity }}</a> {% else %} <a href="{{ sort_quantity }}">{{ column_quantity }}</a> {% endif %}</td>*/
/*                       <td class="text-left">{% if sort == 'p.status' %} <a href="{{ sort_status }}" class="{{ order|lower }}">{{ column_status }}</a> {% else %} <a href="{{ sort_status }}">{{ column_status }}</a> {% endif %}</td>*/
/*                       <td class="text-right">{{ column_action }}</td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                   */
/*                   {% if products %}*/
/*                   {% for product in products %}*/
/*                   <tr>*/
/*                     <td class="text-center">{% if product.product_id in selected %}*/
/*                       <input type="checkbox" name="selected[]" value="{{ product.product_id }}" checked="checked" />*/
/*                       {% else %}*/
/*                       <input type="checkbox" name="selected[]" value="{{ product.product_id }}" />*/
/*                       {% endif %}</td>*/
/*                     <td class="text-center">{% if product.image %} <img src="{{ product.image }}" alt="{{ product.name }}" class="img-thumbnail" /> {% else %} <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span> {% endif %}</td>*/
/*                     */
/* 			<td class="text-left">{{ product.name }} <a href="{{ product.front_link }}" data-toggle="tooltip" title="{{ button_view }}" TARGET="_BLANK"><i class="fa fa-external-link"></i></a></td>*/
/* 				*/
/*                     <td class="text-left">{{ product.model }}</td>*/
/*                     <td class="text-right">{% if product.special %} <span style="text-decoration: line-through;">{{ product.price }}</span><br/>*/
/*                       <div class="text-danger">{{ product.special }}</div>*/
/*                       {% else %}*/
/*                       */
/* 				<div id="show_price_input_{{ product.product_id }}" onclick="show_input('{{ product.product_id }}')">{{ product.raw_price }}</div>*/
/* 				<input type="text" value="{{ product.raw_price }}" placeholder="price" id="price_{{ product.product_id }}" class="form-control input-sm" style="display:none;" />*/
/* 				<div id="button_price_{{ product.product_id }}">*/
/* 					<span class="btn btn-primary btn-xs edit" onclick="edit_price_input('{{ product.product_id }}')"><i class="fa fa-pencil"></i></span>*/
/* 					<span class="btn btn-primary btn-xs save" style="display:none" onclick="save_price_input('{{ product.product_id }}')"><i class="fa fa-check"></i></span>*/
/* 					<span class="btn btn-default btn-xs cancel" style="display:none" onclick="cancel_price_input('{{ product.product_id }}')"><i class="fa fa-times"></i></span>*/
/* 				</div>*/
/* 				*/
/*                       {% endif %}</td>*/
/*                     */
/* 			<td class="text-right">*/
/* 				<div id="show_quantity_input_{{ product.product_id }}">*/
/* 					{% if product.quantity <= 0 %} <span class="label label-warning">{{ product.quantity }}</span> {% elseif product.quantity <= 5 %} <span class="label label-danger">{{ product.quantity }}</span> {% else %} <span class="label label-success">{{ product.quantity }}</span> {% endif %} */
/* 				</div>*/
/* 				<input type="number" value="{{ product.quantity }}" placeholder="quantity" id="quantity_{{ product.product_id }}" class="form-control input-sm" style="display:none;width:90px;" />*/
/* 				<div id="button_quantity_{{ product.product_id }}">*/
/* 					<span class="btn btn-primary btn-xs edit" onclick="edit_quantity_input('{{ product.product_id }}')"><i class="fa fa-pencil"></i></span>*/
/* 					<span class="btn btn-primary btn-xs save" style="display:none" onclick="save_quantity_input('{{ product.product_id }}')"><i class="fa fa-check"></i></span>*/
/* 					<span class="btn btn-default btn-xs cancel" style="display:none" onclick="cancel_quantity_input('{{ product.product_id }}')"><i class="fa fa-times"></i></span>*/
/* 				</div>*/
/* 			</td>*/
/* 				*/
/*                     */
/* 			<td class="text-left">*/
/* 				<div id="status_{{ product.product_id }}">*/
/* 					<div>*/
/* 					{% if product.status == 1 %}*/
/*  						 {{ "<div class='enabled'><i class='fa fa-check'></i> <b>Enabled</b></div>" }}*/
/* 					{% else %}*/
/* 						 {{ "<div class='disabled'><i class='fa fa-times'></i> <b>Disabled</b></div>" }}*/
/* 					{% endif %}*/
/* 					</div>  */
/* 					  {% if product.status == 1 %}*/
/* 					  <a class="status_{{ product.product_id }} small" onclick="change_status('{{ product.product_id }}','0')" data-toggle="tooltip" title="Click to  {{ text_disabled }} " ><i class='fa fa-times'></i> {{ text_disabled}} </a>*/
/* 					 {% else %}*/
/* 					  <a class="status_{{ product.product_id }} small" onclick="change_status('{{ product.product_id }}','1')" data-toggle="tooltip" title="Click to  {{ text_enabled }} " ><i class='fa fa-check'></i>  {{  text_enabled }}</a>*/
/* 					 {% endif %}*/
/* 				</div>*/
/* 			</td>*/
/* 				*/
/*                     <td class="text-right"><a href="{{ product.edit }}" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>*/
/*                   </tr>*/
/*                   {% endfor %}*/
/*                   {% else %}*/
/*                   <tr>*/
/*                     <td class="text-center" colspan="8">{{ text_no_results }}</td>*/
/*                   </tr>*/
/*                   {% endif %}*/
/*                     </tbody>*/
/*                   */
/*                 </table>*/
/*               </div>*/
/*             </form>*/
/*             <div class="row">*/
/*               <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*               <div class="col-sm-6 text-right">{{ results }}</div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <script type="text/javascript"><!--*/
/* $('#button-filter').on('click', function() {*/
/* 	var url = '';*/
/* */
/* 	var filter_name = $('input[name=\'filter_name\']').val();*/
/* */
/* 	if (filter_name) {*/
/* 		url += '&filter_name=' + encodeURIComponent(filter_name);*/
/* 	}*/
/* */
/* 	var filter_model = $('input[name=\'filter_model\']').val();*/
/* */
/* 	if (filter_model) {*/
/* 		url += '&filter_model=' + encodeURIComponent(filter_model);*/
/* 	}*/
/* */
/* 	var filter_price = $('input[name=\'filter_price\']').val();*/
/* */
/* 	if (filter_price) {*/
/* 		url += '&filter_price=' + encodeURIComponent(filter_price);*/
/* 	}*/
/* */
/* 	var filter_quantity = $('input[name=\'filter_quantity\']').val();*/
/* */
/* 	if (filter_quantity) {*/
/* 		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);*/
/* 	}*/
/* */
/* 	var filter_status = $('select[name=\'filter_status\']').val();*/
/* */
/* 	if (filter_status !== '') {*/
/* 		url += '&filter_status=' + encodeURIComponent(filter_status);*/
/* 	}*/
/* */
/* 	location = 'index.php?route=catalog/product&user_token={{ user_token }}' + url;*/
/* });*/
/* //--></script> */
/*   <script type="text/javascript"><!--*/
/* // IE and Edge fix!*/
/* $('button[form=\'form-product\']').on('click', function(e) {*/
/* 	$('#form-product').attr('action', $(this).attr('formaction'));*/
/* });*/
/*   */
/* $('input[name=\'filter_name\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_name\']').val(item['label']);*/
/* 	}*/
/* });*/
/* */
/* $('input[name=\'filter_model\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_model=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['model'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_model\']').val(item['label']);*/
/* 	}*/
/* });*/
/* //--></script></div>*/
/* */
/* 				  <script type="text/javascript">*/
/* 					function change_status(id,statusid){*/
/* 						$('#status_'+id).addClass("loader16");*/
/* 						$('.status_'+id).remove();*/
/* 						$.ajax({*/
/* 							url: 'index.php?route=catalog/product/changestatusid&user_token={{ user_token }}&product_id='+id+'&status='+statusid,*/
/* 							success: function(data) {*/
/* 							$('#status_'+id).html(data);*/
/* 							$('#status_'+id).removeClass("loader16");*/
/* 							}*/
/* 						  });*/
/* 					}*/
/* 					function save_price_input(id){*/
/* 						$('#price_'+id).prop("disabled",true);*/
/* 						var price = $('#price_'+id).val();*/
/* 						$.ajax({*/
/* 							url: 'index.php?route=catalog/product/changeproductprice&user_token={{ user_token }}&product_id='+id+'&price='+price,*/
/* 							complete: function () {*/
/* 								$('#button_price_'+id+' .save').hide();*/
/* 								$('#button_price_'+id+' .cancel').hide();*/
/* 								$('#button_price_'+id+' .edit').show();*/
/* 							},*/
/* 							success: function(data) {*/
/* 							$('#price_'+id).prop("disabled",false);*/
/* 							$('#show_price_input_'+id).css("display","block");*/
/* 							$('#show_price_input_'+id).empty();*/
/* 							$('#show_price_input_'+id).append(data);*/
/* 							$('#price_'+id).css("display","none");*/
/* 							}*/
/* 						  });*/
/* 					}*/
/* 					function edit_price_input(id){*/
/* 						$('#price_'+id).css("display","block");*/
/* 						$('#show_price_input_'+id).css("display","none");*/
/* 						$('#button_price_'+ id +' .save').show();*/
/* 						$('#button_price_'+ id +' .cancel').show();*/
/* 						$('#button_price_'+ id +' .edit').hide();*/
/* 					}*/
/* 					function cancel_price_input(id){*/
/* 						$('#price_'+id).css("display","none");*/
/* 						$('#show_price_input_'+id).css("display","block");*/
/* 						$('#button_price_'+id+' .save').hide();*/
/* 						$('#button_price_'+id+' .cancel').hide();*/
/* 						$('#button_price_'+id+' .edit').show();*/
/* 					}*/
/* 					*/
/* 					function save_quantity_input(id){*/
/* 						$('#quantity_'+id).prop("disabled",true);*/
/* 						var quantity = $('#quantity_'+id).val();*/
/* 						$.ajax({*/
/* 							url: 'index.php?route=catalog/product/changeproductquantity&user_token={{ user_token }}&product_id='+id+'&quantity='+quantity,*/
/* 							complete: function () {*/
/* 								$('#button_quantity_'+id+' .save').hide();*/
/* 								$('#button_quantity_'+id+' .cancel').hide();*/
/* 								$('#button_quantity_'+id+' .edit').show();*/
/* 							},*/
/* 							success: function(data) {*/
/* 							$('#quantity_'+id).prop("disabled",false);*/
/* 							$('#show_quantity_input_'+id).css("display","block");*/
/* 							$('#show_quantity_input_'+id).empty();*/
/* 							if(data <= 0){*/
/* 								$('#show_quantity_input_'+id).append('<span class="label label-warning">' + data + '</span>');*/
/* 							} else if(data <= 5) {*/
/* 								$('#show_quantity_input_'+id).append('<span class="label label-danger">' + data + '</span>');*/
/* 							} else {*/
/* 								$('#show_quantity_input_'+id).append('<span class="label label-success">' + data + '</span>');*/
/* 							}*/
/* 							$('#quantity_'+id).css("display","none");*/
/* 							}*/
/* 						  });*/
/* 					}*/
/* 					function edit_quantity_input(id){*/
/* 						$('#quantity_'+id).css("display","block");*/
/* 						$('#show_quantity_input_'+id).css("display","none");*/
/* 						$('#button_quantity_'+ id +' .save').show();*/
/* 						$('#button_quantity_'+ id +' .cancel').show();*/
/* 						$('#button_quantity_'+ id +' .edit').hide();*/
/* 					}*/
/* 					function cancel_quantity_input(id){*/
/* 						$('#quantity_'+id).css("display","none");*/
/* 						$('#show_quantity_input_'+id).css("display","block");*/
/* 						$('#button_quantity_'+id+' .save').hide();*/
/* 						$('#button_quantity_'+id+' .cancel').hide();*/
/* 						$('#button_quantity_'+id+' .edit').show();*/
/* 					}*/
/* 				</script>*/
/* 				<style>*/
/* 					.enabled  {*/
/* 					color: #0fa000;*/
/* 				}*/
/* 				.disabled  {*/
/* 					color: #ff0000;*/
/* 				}*/
/* 				</style>*/
/* {{ footer }} */
