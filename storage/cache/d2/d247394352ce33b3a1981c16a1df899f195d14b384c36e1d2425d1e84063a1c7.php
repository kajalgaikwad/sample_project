<?php

/* catalog/review_list.twig */
class __TwigTemplate_a199967ec2634701862b066d097f00a7b0b30195a78fe46fb83a47900d3522db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">

        <div class=\"btn-group\">
          ";
        // line 8
        if ((isset($context["information"]) ? $context["information"] : null)) {
            // line 9
            echo "          <span class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["entry_review_information"]) ? $context["entry_review_information"] : null);
            echo "\"><i class=\"fa fa-file-text-o fa-fw\"></i></span>
          <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
            // line 10
            echo (isset($context["entry_review_information"]) ? $context["entry_review_information"] : null);
            echo " <span class=\"caret\"></span></button>
          <ul class=\"dropdown-menu dropdown-menu-right\"><li><a href=\"";
            // line 11
            echo (isset($context["type_url"]) ? $context["type_url"] : null);
            echo "\"><i class=\"fa fa-shopping-cart fa-fw\"></i> ";
            echo (isset($context["entry_review_product"]) ? $context["entry_review_product"] : null);
            echo "</a></li></ul>
          ";
        } else {
            // line 13
            echo "          <span class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["entry_review_product"]) ? $context["entry_review_product"] : null);
            echo "\"><i class=\"fa fa-shopping-cart fa-fw\"></i></span>
          <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
            // line 14
            echo (isset($context["entry_review_product"]) ? $context["entry_review_product"] : null);
            echo " <span class=\"caret\"></span></button>
          <ul class=\"dropdown-menu dropdown-menu-right\"><li><a href=\"";
            // line 15
            echo (isset($context["type_url"]) ? $context["type_url"] : null);
            echo "\"><i class=\"fa fa-file-text-o fa-fw\"></i> ";
            echo (isset($context["entry_review_information"]) ? $context["entry_review_information"] : null);
            echo "</a></li></ul>
          ";
        }
        // line 17
        echo "        </div>
        
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 19
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" onclick=\"\$('#filter-review').toggleClass('hidden-sm hidden-xs');\" class=\"btn btn-default hidden-md hidden-lg\"><i class=\"fa fa-filter\"></i></button>
        <a href=\"";
        // line 20
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 21
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-review').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 23
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 26
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">";
        // line 31
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 32
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 36
        echo "    ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 37
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 41
        echo "    <div class=\"row\">
      <div id=\"filter-review\" class=\"col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs\">
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h3 class=\"panel-title\"><i class=\"fa fa-filter\"></i> ";
        // line 45
        echo (isset($context["text_filter"]) ? $context["text_filter"] : null);
        echo "</h3>
          </div>
          <div  class=\"panel-body\">
            <div class=\"form-group\">

              ";
        // line 50
        if ((isset($context["information"]) ? $context["information"] : null)) {
            // line 51
            echo "              <label class=\"control-label\" for=\"input-information\">";
            echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
            echo "</label>
              <input type=\"text\" name=\"filter_information\" value=\"";
            // line 52
            echo (isset($context["filter_information"]) ? $context["filter_information"] : null);
            echo "\" placeholder=\"";
            echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
            echo "\" id=\"input-product\" class=\"form-control\" />
              ";
        } else {
            // line 54
            echo "        
              <label class=\"control-label\" for=\"input-product\">";
            // line 55
            echo (isset($context["entry_product"]) ? $context["entry_product"] : null);
            echo "</label>
              <input type=\"text\" name=\"filter_product\" value=\"";
            // line 56
            echo (isset($context["filter_product"]) ? $context["filter_product"] : null);
            echo "\" placeholder=\"";
            echo (isset($context["entry_product"]) ? $context["entry_product"] : null);
            echo "\" id=\"input-product\" class=\"form-control\" />

              ";
        }
        // line 59
        echo "        
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-author\">";
        // line 62
        echo (isset($context["entry_author"]) ? $context["entry_author"] : null);
        echo "</label>
              <input type=\"text\" name=\"filter_author\" value=\"";
        // line 63
        echo (isset($context["filter_author"]) ? $context["filter_author"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_author"]) ? $context["entry_author"] : null);
        echo "\" id=\"input-author\" class=\"form-control\" />
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-status\">";
        // line 66
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
              <select name=\"filter_status\" id=\"input-status\" class=\"form-control\">
                <option value=\"\"></option>
                
                  
              
              
          
          
                  
                  
                  
                  ";
        // line 78
        if (((isset($context["filter_status"]) ? $context["filter_status"] : null) == "1")) {
            // line 79
            echo "                  
                  
                  
                  
          
          
              
              
                  
                <option value=\"1\" selected=\"selected\">";
            // line 88
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                
                  
              
              
          
          
                  
                  
                  
                  ";
        } else {
            // line 99
            echo "                  
                  
                  
                  
          
          
              
              
                  
                <option value=\"1\">";
            // line 108
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                
                  
              
              
          
          
                  
                  
                  
                  ";
        }
        // line 119
        echo "                  ";
        if (((isset($context["filter_status"]) ? $context["filter_status"] : null) == "0")) {
            // line 120
            echo "                  
                  
                  
                  
          
          
              
              
                  
                <option value=\"0\" selected=\"selected\">";
            // line 129
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                
                  
              
              
          
          
                  
                  
                  
                  ";
        } else {
            // line 140
            echo "                  
                  
                  
                  
          
          
              
              
                  
                <option value=\"0\">";
            // line 149
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                
                  ";
        }
        // line 152
        echo "                
              </select>
            </div>
            <div class=\"form-group\">
              <label class=\"control-label\" for=\"input-date-added\">";
        // line 156
        echo (isset($context["entry_date_added"]) ? $context["entry_date_added"] : null);
        echo "</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"filter_date_added\" value=\"";
        // line 158
        echo (isset($context["filter_date_added"]) ? $context["filter_date_added"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_date_added"]) ? $context["entry_date_added"] : null);
        echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-date-added\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            <div class=\"form-group text-right\">
              <button type=\"button\" id=\"button-filter\" class=\"btn btn-default\"><i class=\"fa fa-filter\"></i> ";
        // line 164
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "</button>
            </div>
          </div>
        </div>
      </div>
      <div class=\"col-md-9 col-md-pull-3 col-sm-12\">
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 172
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
          </div>
          <div class=\"panel-body\">
            <form action=\"";
        // line 175
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-review\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>

                      ";
        // line 182
        if ((isset($context["information"]) ? $context["information"] : null)) {
            // line 183
            echo "                      <td class=\"text-left\">";
            if (((isset($context["sort"]) ? $context["sort"] : null) == "id.title")) {
                echo " <a href=\"";
                echo (isset($context["sort_information"]) ? $context["sort_information"] : null);
                echo "\" class=\"";
                echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
                echo "\">";
                echo (isset($context["column_information"]) ? $context["column_information"] : null);
                echo "</a> ";
            } else {
                echo " <a href=\"";
                echo (isset($context["sort_information"]) ? $context["sort_information"] : null);
                echo "\">";
                echo (isset($context["column_information"]) ? $context["column_information"] : null);
                echo "</a> ";
            }
            echo "</td>
                      ";
        } else {
            // line 185
            echo "        
                      <td class=\"text-left\">";
            // line 186
            if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
                echo " <a href=\"";
                echo (isset($context["sort_product"]) ? $context["sort_product"] : null);
                echo "\" class=\"";
                echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
                echo "\">";
                echo (isset($context["column_product"]) ? $context["column_product"] : null);
                echo "</a> ";
            } else {
                echo " <a href=\"";
                echo (isset($context["sort_product"]) ? $context["sort_product"] : null);
                echo "\">";
                echo (isset($context["column_product"]) ? $context["column_product"] : null);
                echo "</a> ";
            }
            echo "</td>

                      ";
        }
        // line 189
        echo "        
                      <td class=\"text-left\">";
        // line 190
        if (((isset($context["sort"]) ? $context["sort"] : null) == "r.author")) {
            echo " <a href=\"";
            echo (isset($context["sort_author"]) ? $context["sort_author"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_author"]) ? $context["column_author"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_author"]) ? $context["sort_author"] : null);
            echo "\">";
            echo (isset($context["column_author"]) ? $context["column_author"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-right\">";
        // line 191
        if (((isset($context["sort"]) ? $context["sort"] : null) == "r.rating")) {
            echo " <a href=\"";
            echo (isset($context["sort_rating"]) ? $context["sort_rating"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_rating"]) ? $context["column_rating"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_rating"]) ? $context["sort_rating"] : null);
            echo "\">";
            echo (isset($context["column_rating"]) ? $context["column_rating"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-left\">";
        // line 192
        if (((isset($context["sort"]) ? $context["sort"] : null) == "r.status")) {
            echo " <a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\">";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-left\">";
        // line 193
        if (((isset($context["sort"]) ? $context["sort"] : null) == "r.date_added")) {
            echo " <a href=\"";
            echo (isset($context["sort_date_added"]) ? $context["sort_date_added"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_date_added"]) ? $context["column_date_added"] : null);
            echo "</a> ";
        } else {
            echo " <a href=\"";
            echo (isset($context["sort_date_added"]) ? $context["sort_date_added"] : null);
            echo "\">";
            echo (isset($context["column_date_added"]) ? $context["column_date_added"] : null);
            echo "</a> ";
        }
        echo "</td>
                      <td class=\"text-right\">";
        // line 194
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                  
                  ";
        // line 199
        if ((isset($context["reviews"]) ? $context["reviews"] : null)) {
            // line 200
            echo "                  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 201
                echo "                  <tr>
                    <td class=\"text-center\">";
                // line 202
                if (twig_in_filter($this->getAttribute($context["review"], "review_id", array()), (isset($context["selected"]) ? $context["selected"] : null))) {
                    // line 203
                    echo "                      <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["review"], "review_id", array());
                    echo "\" checked=\"checked\" />
                      ";
                } else {
                    // line 205
                    echo "                      <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["review"], "review_id", array());
                    echo "\" />
                      ";
                }
                // line 206
                echo "</td>
                    
                    ";
                // line 208
                if ((isset($context["information"]) ? $context["information"] : null)) {
                    // line 209
                    echo "                    <td class=\"text-left\">";
                    echo $this->getAttribute($context["review"], "title", array());
                    echo "</td>
                    ";
                } else {
                    // line 211
                    echo "                    <td class=\"text-left\">";
                    echo $this->getAttribute($context["review"], "name", array());
                    echo "</td>
                    ";
                }
                // line 213
                echo "        
                    <td class=\"text-left\">";
                // line 214
                echo $this->getAttribute($context["review"], "author", array());
                echo "</td>
                    <td class=\"text-right\">";
                // line 215
                echo $this->getAttribute($context["review"], "rating", array());
                echo "</td>
                    <td class=\"text-left\">";
                // line 216
                echo $this->getAttribute($context["review"], "status", array());
                echo "</td>
                    <td class=\"text-left\">";
                // line 217
                echo $this->getAttribute($context["review"], "date_added", array());
                echo "</td>
                    <td class=\"text-right\"><a href=\"";
                // line 218
                echo $this->getAttribute($context["review"], "edit", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                  </tr>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 221
            echo "                  ";
        } else {
            // line 222
            echo "                  <tr>
                    <td class=\"text-center\" colspan=\"7\">";
            // line 223
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
                  </tr>
                  ";
        }
        // line 226
        echo "                    </tbody>
                  
                </table>
              </div>
            </form>
            <div class=\"row\">
              <div class=\"col-sm-6 text-left\">";
        // line 232
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
              <div class=\"col-sm-6 text-right\">";
        // line 233
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
\$('#button-filter').on('click', function() {
\turl = 'index.php?route=catalog/review&user_token=";
        // line 242
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
\t

    ";
        // line 245
        if ((isset($context["information"]) ? $context["information"] : null)) {
            // line 246
            echo "\turl += '&information=1';

\tvar filter_information = \$('input[name=\\'filter_information\\']').val();
\t
\tif (filter_information) {
\t\turl += '&filter_information=' + encodeURIComponent(filter_information);
\t}
    ";
        } else {
            // line 254
            echo "        
\tvar filter_product = \$('input[name=\\'filter_product\\']').val();
\t
\tif (filter_product) {
\t\turl += '&filter_product=' + encodeURIComponent(filter_product);
\t}
\t

    ";
        }
        // line 263
        echo "        
\tvar filter_author = \$('input[name=\\'filter_author\\']').val();
\t
\tif (filter_author) {
\t\turl += '&filter_author=' + encodeURIComponent(filter_author);
\t}
\t
\tvar filter_status = \$('select[name=\\'filter_status\\']').val();
\t
\tif (filter_status !== '') {
\t\turl += '&filter_status=' + encodeURIComponent(filter_status); 
\t}\t\t
\t\t\t
\tvar filter_date_added = \$('input[name=\\'filter_date_added\\']').val();
\t
\tif (filter_date_added) {
\t\turl += '&filter_date_added=' + encodeURIComponent(filter_date_added);
\t}

\tlocation = url;
});
//--></script> 
  <script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: '";
        // line 287
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickTime: false
});
//--></script></div>
";
        // line 291
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "catalog/review_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  650 => 291,  643 => 287,  617 => 263,  606 => 254,  596 => 246,  594 => 245,  588 => 242,  576 => 233,  572 => 232,  564 => 226,  558 => 223,  555 => 222,  552 => 221,  541 => 218,  537 => 217,  533 => 216,  529 => 215,  525 => 214,  522 => 213,  516 => 211,  510 => 209,  508 => 208,  504 => 206,  498 => 205,  492 => 203,  490 => 202,  487 => 201,  482 => 200,  480 => 199,  472 => 194,  454 => 193,  436 => 192,  418 => 191,  400 => 190,  397 => 189,  377 => 186,  374 => 185,  354 => 183,  352 => 182,  342 => 175,  336 => 172,  325 => 164,  314 => 158,  309 => 156,  303 => 152,  297 => 149,  286 => 140,  272 => 129,  261 => 120,  258 => 119,  244 => 108,  233 => 99,  219 => 88,  208 => 79,  206 => 78,  191 => 66,  183 => 63,  179 => 62,  174 => 59,  166 => 56,  162 => 55,  159 => 54,  152 => 52,  147 => 51,  145 => 50,  137 => 45,  131 => 41,  123 => 37,  120 => 36,  112 => 32,  110 => 31,  105 => 28,  94 => 26,  90 => 25,  85 => 23,  78 => 21,  72 => 20,  68 => 19,  64 => 17,  57 => 15,  53 => 14,  48 => 13,  41 => 11,  37 => 10,  32 => 9,  30 => 8,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/* */
/*         <div class="btn-group">*/
/*           {% if information %}*/
/*           <span class="btn btn-default" data-toggle="tooltip" title="{{ entry_review_information }}"><i class="fa fa-file-text-o fa-fw"></i></span>*/
/*           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ entry_review_information }} <span class="caret"></span></button>*/
/*           <ul class="dropdown-menu dropdown-menu-right"><li><a href="{{ type_url }}"><i class="fa fa-shopping-cart fa-fw"></i> {{ entry_review_product }}</a></li></ul>*/
/*           {% else %}*/
/*           <span class="btn btn-default" data-toggle="tooltip" title="{{ entry_review_product }}"><i class="fa fa-shopping-cart fa-fw"></i></span>*/
/*           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ entry_review_product }} <span class="caret"></span></button>*/
/*           <ul class="dropdown-menu dropdown-menu-right"><li><a href="{{ type_url }}"><i class="fa fa-file-text-o fa-fw"></i> {{ entry_review_information }}</a></li></ul>*/
/*           {% endif %}*/
/*         </div>*/
/*         */
/*         <button type="button" data-toggle="tooltip" title="{{ button_filter }}" onclick="$('#filter-review').toggleClass('hidden-sm hidden-xs');" class="btn btn-default hidden-md hidden-lg"><i class="fa fa-filter"></i></button>*/
/*         <a href="{{ add }}" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/*         <button type="button" data-toggle="tooltip" title="{{ button_delete }}" class="btn btn-danger" onclick="confirm('{{ text_confirm }}') ? $('#form-review').submit() : false;"><i class="fa fa-trash-o"></i></button>*/
/*       </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">{% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     {% if success %}*/
/*     <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="row">*/
/*       <div id="filter-review" class="col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs">*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h3 class="panel-title"><i class="fa fa-filter"></i> {{ text_filter }}</h3>*/
/*           </div>*/
/*           <div  class="panel-body">*/
/*             <div class="form-group">*/
/* */
/*               {% if information %}*/
/*               <label class="control-label" for="input-information">{{ entry_information }}</label>*/
/*               <input type="text" name="filter_information" value="{{ filter_information }}" placeholder="{{ entry_information }}" id="input-product" class="form-control" />*/
/*               {% else %}*/
/*         */
/*               <label class="control-label" for="input-product">{{ entry_product }}</label>*/
/*               <input type="text" name="filter_product" value="{{ filter_product }}" placeholder="{{ entry_product }}" id="input-product" class="form-control" />*/
/* */
/*               {% endif %}*/
/*         */
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-author">{{ entry_author }}</label>*/
/*               <input type="text" name="filter_author" value="{{ filter_author }}" placeholder="{{ entry_author }}" id="input-author" class="form-control" />*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-status">{{ entry_status }}</label>*/
/*               <select name="filter_status" id="input-status" class="form-control">*/
/*                 <option value=""></option>*/
/*                 */
/*                   */
/*               */
/*               */
/*           */
/*           */
/*                   */
/*                   */
/*                   */
/*                   {% if filter_status == '1' %}*/
/*                   */
/*                   */
/*                   */
/*                   */
/*           */
/*           */
/*               */
/*               */
/*                   */
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 */
/*                   */
/*               */
/*               */
/*           */
/*           */
/*                   */
/*                   */
/*                   */
/*                   {% else %}*/
/*                   */
/*                   */
/*                   */
/*                   */
/*           */
/*           */
/*               */
/*               */
/*                   */
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 */
/*                   */
/*               */
/*               */
/*           */
/*           */
/*                   */
/*                   */
/*                   */
/*                   {% endif %}*/
/*                   {% if filter_status == '0' %}*/
/*                   */
/*                   */
/*                   */
/*                   */
/*           */
/*           */
/*               */
/*               */
/*                   */
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 */
/*                   */
/*               */
/*               */
/*           */
/*           */
/*                   */
/*                   */
/*                   */
/*                   {% else %}*/
/*                   */
/*                   */
/*                   */
/*                   */
/*           */
/*           */
/*               */
/*               */
/*                   */
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 */
/*                   {% endif %}*/
/*                 */
/*               </select>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="control-label" for="input-date-added">{{ entry_date_added }}</label>*/
/*               <div class="input-group date">*/
/*                 <input type="text" name="filter_date_added" value="{{ filter_date_added }}" placeholder="{{ entry_date_added }}" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />*/
/*                 <span class="input-group-btn">*/
/*                 <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                 </span></div>*/
/*             </div>*/
/*             <div class="form-group text-right">*/
/*               <button type="button" id="button-filter" class="btn btn-default"><i class="fa fa-filter"></i> {{ button_filter }}</button>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="col-md-9 col-md-pull-3 col-sm-12">*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/*           </div>*/
/*           <div class="panel-body">*/
/*             <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-review">*/
/*               <div class="table-responsive">*/
/*                 <table class="table table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/* */
/*                       {% if information %}*/
/*                       <td class="text-left">{% if sort == 'id.title' %} <a href="{{ sort_information }}" class="{{ order|lower }}">{{ column_information }}</a> {% else %} <a href="{{ sort_information }}">{{ column_information }}</a> {% endif %}</td>*/
/*                       {% else %}*/
/*         */
/*                       <td class="text-left">{% if sort == 'pd.name' %} <a href="{{ sort_product }}" class="{{ order|lower }}">{{ column_product }}</a> {% else %} <a href="{{ sort_product }}">{{ column_product }}</a> {% endif %}</td>*/
/* */
/*                       {% endif %}*/
/*         */
/*                       <td class="text-left">{% if sort == 'r.author' %} <a href="{{ sort_author }}" class="{{ order|lower }}">{{ column_author }}</a> {% else %} <a href="{{ sort_author }}">{{ column_author }}</a> {% endif %}</td>*/
/*                       <td class="text-right">{% if sort == 'r.rating' %} <a href="{{ sort_rating }}" class="{{ order|lower }}">{{ column_rating }}</a> {% else %} <a href="{{ sort_rating }}">{{ column_rating }}</a> {% endif %}</td>*/
/*                       <td class="text-left">{% if sort == 'r.status' %} <a href="{{ sort_status }}" class="{{ order|lower }}">{{ column_status }}</a> {% else %} <a href="{{ sort_status }}">{{ column_status }}</a> {% endif %}</td>*/
/*                       <td class="text-left">{% if sort == 'r.date_added' %} <a href="{{ sort_date_added }}" class="{{ order|lower }}">{{ column_date_added }}</a> {% else %} <a href="{{ sort_date_added }}">{{ column_date_added }}</a> {% endif %}</td>*/
/*                       <td class="text-right">{{ column_action }}</td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                   */
/*                   {% if reviews %}*/
/*                   {% for review in reviews %}*/
/*                   <tr>*/
/*                     <td class="text-center">{% if review.review_id in selected %}*/
/*                       <input type="checkbox" name="selected[]" value="{{ review.review_id }}" checked="checked" />*/
/*                       {% else %}*/
/*                       <input type="checkbox" name="selected[]" value="{{ review.review_id }}" />*/
/*                       {% endif %}</td>*/
/*                     */
/*                     {% if information %}*/
/*                     <td class="text-left">{{ review.title }}</td>*/
/*                     {% else %}*/
/*                     <td class="text-left">{{ review.name }}</td>*/
/*                     {% endif %}*/
/*         */
/*                     <td class="text-left">{{ review.author }}</td>*/
/*                     <td class="text-right">{{ review.rating }}</td>*/
/*                     <td class="text-left">{{ review.status }}</td>*/
/*                     <td class="text-left">{{ review.date_added }}</td>*/
/*                     <td class="text-right"><a href="{{ review.edit }}" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>*/
/*                   </tr>*/
/*                   {% endfor %}*/
/*                   {% else %}*/
/*                   <tr>*/
/*                     <td class="text-center" colspan="7">{{ text_no_results }}</td>*/
/*                   </tr>*/
/*                   {% endif %}*/
/*                     </tbody>*/
/*                   */
/*                 </table>*/
/*               </div>*/
/*             </form>*/
/*             <div class="row">*/
/*               <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*               <div class="col-sm-6 text-right">{{ results }}</div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <script type="text/javascript"><!--*/
/* $('#button-filter').on('click', function() {*/
/* 	url = 'index.php?route=catalog/review&user_token={{ user_token }}';*/
/* 	*/
/* */
/*     {% if information %}*/
/* 	url += '&information=1';*/
/* */
/* 	var filter_information = $('input[name=\'filter_information\']').val();*/
/* 	*/
/* 	if (filter_information) {*/
/* 		url += '&filter_information=' + encodeURIComponent(filter_information);*/
/* 	}*/
/*     {% else %}*/
/*         */
/* 	var filter_product = $('input[name=\'filter_product\']').val();*/
/* 	*/
/* 	if (filter_product) {*/
/* 		url += '&filter_product=' + encodeURIComponent(filter_product);*/
/* 	}*/
/* 	*/
/* */
/*     {% endif %}*/
/*         */
/* 	var filter_author = $('input[name=\'filter_author\']').val();*/
/* 	*/
/* 	if (filter_author) {*/
/* 		url += '&filter_author=' + encodeURIComponent(filter_author);*/
/* 	}*/
/* 	*/
/* 	var filter_status = $('select[name=\'filter_status\']').val();*/
/* 	*/
/* 	if (filter_status !== '') {*/
/* 		url += '&filter_status=' + encodeURIComponent(filter_status); */
/* 	}		*/
/* 			*/
/* 	var filter_date_added = $('input[name=\'filter_date_added\']').val();*/
/* 	*/
/* 	if (filter_date_added) {*/
/* 		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);*/
/* 	}*/
/* */
/* 	location = url;*/
/* });*/
/* //--></script> */
/*   <script type="text/javascript"><!--*/
/* $('.date').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickTime: false*/
/* });*/
/* //--></script></div>*/
/* {{ footer }}*/
