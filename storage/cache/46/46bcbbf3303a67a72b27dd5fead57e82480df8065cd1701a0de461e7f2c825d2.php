<?php

/* catalog/category_list.twig */
class __TwigTemplate_495f5626c1c4cd887e6aa617b5854a6c216dfcf066c9bdf41363cdf3eeeea1e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      
      <div class=\"pull-right\">
        <div class=\"btn-group\">
          ";
        // line 8
        if ( !(null === (isset($context["filter_information"]) ? $context["filter_information"] : null))) {
            // line 9
            echo "          <span class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            echo (((isset($context["filter_information"]) ? $context["filter_information"] : null)) ? ((isset($context["text_type_information"]) ? $context["text_type_information"] : null)) : ((isset($context["text_type_product"]) ? $context["text_type_product"] : null)));
            echo "\">
          ";
            // line 10
            echo (((isset($context["filter_information"]) ? $context["filter_information"] : null)) ? ("<i class=\"fa fa-file-text-o fa-fw\"></i>") : ("<i class=\"fa fa-shopping-cart fa-fw\"></i>"));
            echo " 
          ";
        } else {
            // line 12
            echo "          <span class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["text_type_all"]) ? $context["text_type_all"] : null);
            echo "\">
            <i class=\"fa fa-list fa-fw\"></i>
          ";
        }
        // line 15
        echo "          </span>
          <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        // line 16
        echo (isset($context["button_type_category"]) ? $context["button_type_category"] : null);
        echo " <span class=\"caret\"></span></button>
          <ul class=\"dropdown-menu dropdown-menu-right\">
            <li><a href=\"";
        // line 18
        echo (isset($context["type_all"]) ? $context["type_all"] : null);
        echo "\"><i class=\"fa fa-list fa-fw\"></i> ";
        echo (isset($context["text_type_all"]) ? $context["text_type_all"] : null);
        echo "</a></li>
            <li class=\"divider\"></li>
            <li><a href=\"";
        // line 20
        echo (isset($context["type_product"]) ? $context["type_product"] : null);
        echo "\"><i class=\"fa fa-shopping-cart fa-fw\"></i> ";
        echo (isset($context["text_type_product"]) ? $context["text_type_product"] : null);
        echo "</a></li>
            <li><a href=\"";
        // line 21
        echo (isset($context["type_information"]) ? $context["type_information"] : null);
        echo "\"><i class=\"fa fa-file-text-o fa-fw\"></i> ";
        echo (isset($context["text_type_information"]) ? $context["text_type_information"] : null);
        echo "</a></li>
          </ul>
        </div>
        <a href=\"";
        // line 24
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
        <a href=\"";
        // line 25
        echo (isset($context["repair"]) ? $context["repair"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_rebuild"]) ? $context["button_rebuild"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i></a>
        
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 27
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-category').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 29
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 32
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 38
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 39
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 43
        echo "    ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 44
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 48
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 50
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 53
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-category\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
                <tr>
                  <td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                  
                  <td class=\"text-left\">";
        // line 60
        if (((isset($context["sort"]) ? $context["sort"] : null) == "name")) {
            // line 61
            echo "        
                    <a href=\"";
            // line 62
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a>
                    ";
        } else {
            // line 64
            echo "                    <a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a>
                    ";
        }
        // line 65
        echo "</td>

                  <td class=\"text-right\">";
        // line 67
        if (((isset($context["sort"]) ? $context["sort"] : null) == "information")) {
            // line 68
            echo "                    <a href=\"";
            echo (isset($context["sort_information"]) ? $context["sort_information"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_type"]) ? $context["column_type"] : null);
            echo "</a>
                    ";
        } else {
            // line 70
            echo "                    <a href=\"";
            echo (isset($context["sort_information"]) ? $context["sort_information"] : null);
            echo "\">";
            echo (isset($context["column_type"]) ? $context["column_type"] : null);
            echo "</a>
                    ";
        }
        // line 71
        echo "</td>
        
                  <td class=\"text-right\">";
        // line 73
        if (((isset($context["sort"]) ? $context["sort"] : null) == "sort_order")) {
            // line 74
            echo "                    <a href=\"";
            echo (isset($context["sort_sort_order"]) ? $context["sort_sort_order"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_sort_order"]) ? $context["column_sort_order"] : null);
            echo "</a>
                    ";
        } else {
            // line 76
            echo "                    <a href=\"";
            echo (isset($context["sort_sort_order"]) ? $context["sort_sort_order"] : null);
            echo "\">";
            echo (isset($context["column_sort_order"]) ? $context["column_sort_order"] : null);
            echo "</a>
                    ";
        }
        // line 77
        echo "</td>
                  <td class=\"text-right\">";
        // line 78
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
                </tr>
              </thead>
              <tbody>
                ";
        // line 82
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 83
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 84
                echo "                <tr>
                  <td class=\"text-center\">";
                // line 85
                if (twig_in_filter($this->getAttribute($context["category"], "category_id", array()), (isset($context["selected"]) ? $context["selected"] : null))) {
                    // line 86
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["category"], "category_id", array());
                    echo "\" checked=\"checked\" />
                    ";
                } else {
                    // line 88
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["category"], "category_id", array());
                    echo "\" />
                    ";
                }
                // line 89
                echo "</td>
                  <td class=\"text-left\">";
                // line 90
                echo $this->getAttribute($context["category"], "name", array());
                echo "</td>

                  <td class=\"text-right\">";
                // line 92
                echo (($this->getAttribute($context["category"], "information", array())) ? ((isset($context["text_type_information"]) ? $context["text_type_information"] : null)) : ((isset($context["text_type_product"]) ? $context["text_type_product"] : null)));
                echo "</td>
        
                  <td class=\"text-right\">";
                // line 94
                echo $this->getAttribute($context["category"], "sort_order", array());
                echo "</td>
                  <td class=\"text-right\"><a href=\"";
                // line 95
                echo $this->getAttribute($context["category"], "edit", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 98
            echo "                ";
        } else {
            // line 99
            echo "                <tr>
                  <td class=\"text-center\" colspan=\"4\">";
            // line 100
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
                </tr>
                ";
        }
        // line 103
        echo "              </tbody>
            </table>
          </div>
        </form>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 108
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 109
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
        </div>
      </div>
    </div>
  </div>
</div>
";
        // line 115
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "catalog/category_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 115,  320 => 109,  316 => 108,  309 => 103,  303 => 100,  300 => 99,  297 => 98,  286 => 95,  282 => 94,  277 => 92,  272 => 90,  269 => 89,  263 => 88,  257 => 86,  255 => 85,  252 => 84,  247 => 83,  245 => 82,  238 => 78,  235 => 77,  227 => 76,  217 => 74,  215 => 73,  211 => 71,  203 => 70,  193 => 68,  191 => 67,  187 => 65,  179 => 64,  170 => 62,  167 => 61,  165 => 60,  155 => 53,  149 => 50,  145 => 48,  137 => 44,  134 => 43,  126 => 39,  124 => 38,  118 => 34,  107 => 32,  103 => 31,  98 => 29,  91 => 27,  84 => 25,  78 => 24,  70 => 21,  64 => 20,  57 => 18,  52 => 16,  49 => 15,  42 => 12,  37 => 10,  32 => 9,  30 => 8,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       */
/*       <div class="pull-right">*/
/*         <div class="btn-group">*/
/*           {% if filter_information is not null %}*/
/*           <span class="btn btn-default" data-toggle="tooltip" title="{{ filter_information ? text_type_information : text_type_product }}">*/
/*           {{ filter_information ? '<i class="fa fa-file-text-o fa-fw"></i>' : '<i class="fa fa-shopping-cart fa-fw"></i>' }} */
/*           {% else %}*/
/*           <span class="btn btn-default" data-toggle="tooltip" title="{{ text_type_all }}">*/
/*             <i class="fa fa-list fa-fw"></i>*/
/*           {% endif %}*/
/*           </span>*/
/*           <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ button_type_category }} <span class="caret"></span></button>*/
/*           <ul class="dropdown-menu dropdown-menu-right">*/
/*             <li><a href="{{ type_all }}"><i class="fa fa-list fa-fw"></i> {{ text_type_all }}</a></li>*/
/*             <li class="divider"></li>*/
/*             <li><a href="{{ type_product }}"><i class="fa fa-shopping-cart fa-fw"></i> {{ text_type_product }}</a></li>*/
/*             <li><a href="{{ type_information }}"><i class="fa fa-file-text-o fa-fw"></i> {{ text_type_information }}</a></li>*/
/*           </ul>*/
/*         </div>*/
/*         <a href="{{ add }}" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/*         <a href="{{ repair }}" data-toggle="tooltip" title="{{ button_rebuild }}" class="btn btn-default"><i class="fa fa-refresh"></i></a>*/
/*         */
/*         <button type="button" data-toggle="tooltip" title="{{ button_delete }}" class="btn btn-danger" onclick="confirm('{{ text_confirm }}') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>*/
/*       </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     {% if success %}*/
/*     <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-category">*/
/*           <div class="table-responsive">*/
/*             <table class="table table-bordered table-hover">*/
/*               <thead>*/
/*                 <tr>*/
/*                   <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/*                   */
/*                   <td class="text-left">{% if sort == 'name' %}*/
/*         */
/*                     <a href="{{ sort_name }}" class="{{ order|lower }}">{{ column_name }}</a>*/
/*                     {% else %}*/
/*                     <a href="{{ sort_name }}">{{ column_name }}</a>*/
/*                     {% endif %}</td>*/
/* */
/*                   <td class="text-right">{% if sort == 'information' %}*/
/*                     <a href="{{ sort_information }}" class="{{ order|lower }}">{{ column_type }}</a>*/
/*                     {% else %}*/
/*                     <a href="{{ sort_information }}">{{ column_type }}</a>*/
/*                     {% endif %}</td>*/
/*         */
/*                   <td class="text-right">{% if sort == 'sort_order' %}*/
/*                     <a href="{{ sort_sort_order }}" class="{{ order|lower }}">{{ column_sort_order }}</a>*/
/*                     {% else %}*/
/*                     <a href="{{ sort_sort_order }}">{{ column_sort_order }}</a>*/
/*                     {% endif %}</td>*/
/*                   <td class="text-right">{{ column_action }}</td>*/
/*                 </tr>*/
/*               </thead>*/
/*               <tbody>*/
/*                 {% if categories %}*/
/*                 {% for category in categories %}*/
/*                 <tr>*/
/*                   <td class="text-center">{% if category.category_id in selected %}*/
/*                     <input type="checkbox" name="selected[]" value="{{ category.category_id }}" checked="checked" />*/
/*                     {% else %}*/
/*                     <input type="checkbox" name="selected[]" value="{{ category.category_id }}" />*/
/*                     {% endif %}</td>*/
/*                   <td class="text-left">{{ category.name }}</td>*/
/* */
/*                   <td class="text-right">{{ category.information ? text_type_information : text_type_product }}</td>*/
/*         */
/*                   <td class="text-right">{{ category.sort_order }}</td>*/
/*                   <td class="text-right"><a href="{{ category.edit }}" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>*/
/*                 </tr>*/
/*                 {% endfor %}*/
/*                 {% else %}*/
/*                 <tr>*/
/*                   <td class="text-center" colspan="4">{{ text_no_results }}</td>*/
/*                 </tr>*/
/*                 {% endif %}*/
/*               </tbody>*/
/*             </table>*/
/*           </div>*/
/*         </form>*/
/*         <div class="row">*/
/*           <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*           <div class="col-sm-6 text-right">{{ results }}</div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
