<?php

/* extension/information/setting.twig */
class __TwigTemplate_39d87ab50ea399445a7f64b1a82ad2b76a55fed178e57512d2514aa11bf642ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-theme\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-theme\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 29
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
            <li><a href=\"#tab-category\" data-toggle=\"tab\">";
        // line 30
        echo (isset($context["tab_category"]) ? $context["tab_category"] : null);
        echo "</a></li>
            <li><a href=\"#tab-exclusion\" data-toggle=\"tab\">";
        // line 31
        echo (isset($context["tab_exclusion"]) ? $context["tab_exclusion"] : null);
        echo "</a></li>
            <li><a href=\"#tab-image\" data-toggle=\"tab\">";
        // line 32
        echo (isset($context["tab_image"]) ? $context["tab_image"] : null);
        echo "</a></li>
          </ul>
          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <fieldset>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 38
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_setting_status\" id=\"input-status\" class=\"form-control\">
                      ";
        // line 41
        if ((isset($context["information_setting_status"]) ? $context["information_setting_status"] : null)) {
            // line 42
            echo "                      <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\">";
            // line 43
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        } else {
            // line 45
            echo "                      <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\" selected=\"selected\">";
            // line 46
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        }
        // line 48
        echo "                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 52
        echo (isset($context["entry_information_information_author"]) ? $context["entry_information_information_author"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 54
        if ((isset($context["information_information_author"]) ? $context["information_information_author"] : null)) {
            // line 55
            echo "                      <input type=\"radio\" name=\"information_information_author\" value=\"1\" checked=\"checked\" />
                      ";
            // line 56
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 58
            echo "                      <input type=\"radio\" name=\"information_information_author\" value=\"1\" />
                      ";
            // line 59
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 60
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 61
        if ( !(isset($context["information_information_author"]) ? $context["information_information_author"] : null)) {
            // line 62
            echo "                      <input type=\"radio\" name=\"information_information_author\" value=\"0\" checked=\"checked\" />
                      ";
            // line 63
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 65
            echo "                      <input type=\"radio\" name=\"information_information_author\" value=\"0\" />
                      ";
            // line 66
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 67
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 71
        echo (isset($context["entry_information_information_date"]) ? $context["entry_information_information_date"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 73
        if ((isset($context["information_information_date"]) ? $context["information_information_date"] : null)) {
            // line 74
            echo "                      <input type=\"radio\" name=\"information_information_date\" value=\"1\" checked=\"checked\" />
                      ";
            // line 75
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 77
            echo "                      <input type=\"radio\" name=\"information_information_date\" value=\"1\" />
                      ";
            // line 78
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 79
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 80
        if ( !(isset($context["information_information_date"]) ? $context["information_information_date"] : null)) {
            // line 81
            echo "                      <input type=\"radio\" name=\"information_information_date\" value=\"0\" checked=\"checked\" />
                      ";
            // line 82
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 84
            echo "                      <input type=\"radio\" name=\"information_information_date\" value=\"0\" />
                      ";
            // line 85
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 86
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 90
        echo (isset($context["entry_information_information_review"]) ? $context["entry_information_information_review"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 92
        if ((isset($context["information_information_review"]) ? $context["information_information_review"] : null)) {
            // line 93
            echo "                      <input type=\"radio\" name=\"information_information_review\" value=\"1\" checked=\"checked\" />
                      ";
            // line 94
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 96
            echo "                      <input type=\"radio\" name=\"information_information_review\" value=\"1\" />
                      ";
            // line 97
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 98
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 99
        if ( !(isset($context["information_information_review"]) ? $context["information_information_review"] : null)) {
            // line 100
            echo "                      <input type=\"radio\" name=\"information_information_review\" value=\"0\" checked=\"checked\" />
                      ";
            // line 101
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 103
            echo "                      <input type=\"radio\" name=\"information_information_review\" value=\"0\" />
                      ";
            // line 104
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 105
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 109
        echo (isset($context["entry_information_information_manufacturer"]) ? $context["entry_information_information_manufacturer"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 111
        if ((isset($context["information_information_manufacturer"]) ? $context["information_information_manufacturer"] : null)) {
            // line 112
            echo "                      <input type=\"radio\" name=\"information_information_manufacturer\" value=\"1\" checked=\"checked\" />
                      ";
            // line 113
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 115
            echo "                      <input type=\"radio\" name=\"information_information_manufacturer\" value=\"1\" />
                      ";
            // line 116
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 117
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 118
        if ( !(isset($context["information_information_manufacturer"]) ? $context["information_information_manufacturer"] : null)) {
            // line 119
            echo "                      <input type=\"radio\" name=\"information_information_manufacturer\" value=\"0\" checked=\"checked\" />
                      ";
            // line 120
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 122
            echo "                      <input type=\"radio\" name=\"information_information_manufacturer\" value=\"0\" />
                      ";
            // line 123
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 124
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 129
        echo (isset($context["text_review"]) ? $context["text_review"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 131
        echo (isset($context["help_review"]) ? $context["help_review"] : null);
        echo "\">";
        echo (isset($context["entry_review"]) ? $context["entry_review"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 133
        if ((isset($context["information_review_status"]) ? $context["information_review_status"] : null)) {
            // line 134
            echo "                      <input type=\"radio\" name=\"information_review_status\" value=\"1\" checked=\"checked\" />
                      ";
            // line 135
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 137
            echo "                      <input type=\"radio\" name=\"information_review_status\" value=\"1\" />
                      ";
            // line 138
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 139
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 140
        if ( !(isset($context["information_review_status"]) ? $context["information_review_status"] : null)) {
            // line 141
            echo "                      <input type=\"radio\" name=\"information_review_status\" value=\"0\" checked=\"checked\" />
                      ";
            // line 142
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 144
            echo "                      <input type=\"radio\" name=\"information_review_status\" value=\"0\" />
                      ";
            // line 145
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 146
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 150
        echo (isset($context["help_review_guest"]) ? $context["help_review_guest"] : null);
        echo "\">";
        echo (isset($context["entry_review_guest"]) ? $context["entry_review_guest"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 152
        if ((isset($context["information_review_guest"]) ? $context["information_review_guest"] : null)) {
            // line 153
            echo "                      <input type=\"radio\" name=\"information_review_guest\" value=\"1\" checked=\"checked\" />
                      ";
            // line 154
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 156
            echo "                      <input type=\"radio\" name=\"information_review_guest\" value=\"1\" />
                      ";
            // line 157
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 158
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 159
        if ( !(isset($context["information_review_guest"]) ? $context["information_review_guest"] : null)) {
            // line 160
            echo "                      <input type=\"radio\" name=\"information_review_guest\" value=\"0\" checked=\"checked\" />
                      ";
            // line 161
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 163
            echo "                      <input type=\"radio\" name=\"information_review_guest\" value=\"0\" />
                      ";
            // line 164
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 165
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 169
        echo (isset($context["help_captcha"]) ? $context["help_captcha"] : null);
        echo "\">";
        echo (isset($context["entry_captcha"]) ? $context["entry_captcha"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_captcha\" id=\"input-captcha\" class=\"form-control\">
                      <option value=\"\">";
        // line 172
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "</option>
                      ";
        // line 173
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["captchas"]) ? $context["captchas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["captcha"]) {
            // line 174
            echo "                      ";
            if (($this->getAttribute($context["captcha"], "value", array()) == (isset($context["information_captcha"]) ? $context["information_captcha"] : null))) {
                // line 175
                echo "                      <option value=\"";
                echo $this->getAttribute($context["captcha"], "value", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["captcha"], "text", array());
                echo "</option>
                      ";
            } else {
                // line 177
                echo "                      <option value=\"";
                echo $this->getAttribute($context["captcha"], "value", array());
                echo "\">";
                echo $this->getAttribute($context["captcha"], "text", array());
                echo "</option>
                      ";
            }
            // line 179
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['captcha'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 180
        echo "                    </select>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-category\">
              <fieldset>
                <legend>";
        // line 187
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-show\">";
        // line 189
        echo (isset($context["entry_information_category_show"]) ? $context["entry_information_category_show"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_category_show\" id=\"input-information-category-show\" class=\"form-control\">
                      ";
        // line 192
        if ((isset($context["information_category_show"]) ? $context["information_category_show"] : null)) {
            // line 193
            echo "                      <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\">";
            // line 194
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        } else {
            // line 196
            echo "                      <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\" selected=\"selected\">";
            // line 197
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        }
        // line 199
        echo "                    </select>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-description-limit\"><span data-toggle=\"tooltip\" title=\"";
        // line 203
        echo (isset($context["help_information_description_length"]) ? $context["help_information_description_length"] : null);
        echo "\">";
        echo (isset($context["entry_information_description_length"]) ? $context["entry_information_description_length"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"information_description_length\" value=\"";
        // line 205
        echo (isset($context["information_description_length"]) ? $context["information_description_length"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_information_description_length"]) ? $context["entry_information_description_length"] : null);
        echo "\" id=\"input-description-limit\" class=\"form-control\" />
                    ";
        // line 206
        if ((isset($context["error_information_description_length"]) ? $context["error_information_description_length"] : null)) {
            // line 207
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_information_description_length"]) ? $context["error_information_description_length"] : null);
            echo "</div>
                    ";
        }
        // line 209
        echo "                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 212
        echo (isset($context["help_information_count"]) ? $context["help_information_count"] : null);
        echo "\">";
        echo (isset($context["entry_information_count"]) ? $context["entry_information_count"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 214
        if ((isset($context["information_count"]) ? $context["information_count"] : null)) {
            // line 215
            echo "                      <input type=\"radio\" name=\"information_count\" value=\"1\" checked=\"checked\" />
                      ";
            // line 216
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 218
            echo "                      <input type=\"radio\" name=\"information_count\" value=\"1\" />
                      ";
            // line 219
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 220
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 221
        if ( !(isset($context["information_count"]) ? $context["information_count"] : null)) {
            // line 222
            echo "                      <input type=\"radio\" name=\"information_count\" value=\"0\" checked=\"checked\" />
                      ";
            // line 223
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 225
            echo "                      <input type=\"radio\" name=\"information_count\" value=\"0\" />
                      ";
            // line 226
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 227
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 231
        echo (isset($context["entry_information_category_author"]) ? $context["entry_information_category_author"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 233
        if ((isset($context["information_category_author"]) ? $context["information_category_author"] : null)) {
            // line 234
            echo "                      <input type=\"radio\" name=\"information_category_author\" value=\"1\" checked=\"checked\" />
                      ";
            // line 235
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 237
            echo "                      <input type=\"radio\" name=\"information_category_author\" value=\"1\" />
                      ";
            // line 238
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 239
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 240
        if ( !(isset($context["information_category_author"]) ? $context["information_category_author"] : null)) {
            // line 241
            echo "                      <input type=\"radio\" name=\"information_category_author\" value=\"0\" checked=\"checked\" />
                      ";
            // line 242
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 244
            echo "                      <input type=\"radio\" name=\"information_category_author\" value=\"0\" />
                      ";
            // line 245
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 246
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 250
        echo (isset($context["entry_information_category_date"]) ? $context["entry_information_category_date"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 252
        if ((isset($context["information_category_date"]) ? $context["information_category_date"] : null)) {
            // line 253
            echo "                      <input type=\"radio\" name=\"information_category_date\" value=\"1\" checked=\"checked\" />
                      ";
            // line 254
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 256
            echo "                      <input type=\"radio\" name=\"information_category_date\" value=\"1\" />
                      ";
            // line 257
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 258
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 259
        if ( !(isset($context["information_category_date"]) ? $context["information_category_date"] : null)) {
            // line 260
            echo "                      <input type=\"radio\" name=\"information_category_date\" value=\"0\" checked=\"checked\" />
                      ";
            // line 261
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 263
            echo "                      <input type=\"radio\" name=\"information_category_date\" value=\"0\" />
                      ";
            // line 264
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 265
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 269
        echo (isset($context["entry_information_category_review"]) ? $context["entry_information_category_review"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 271
        if ((isset($context["information_category_review"]) ? $context["information_category_review"] : null)) {
            // line 272
            echo "                      <input type=\"radio\" name=\"information_category_review\" value=\"1\" checked=\"checked\" />
                      ";
            // line 273
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        } else {
            // line 275
            echo "                      <input type=\"radio\" name=\"information_category_review\" value=\"1\" />
                      ";
            // line 276
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                      ";
        }
        // line 277
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 278
        if ( !(isset($context["information_category_review"]) ? $context["information_category_review"] : null)) {
            // line 279
            echo "                      <input type=\"radio\" name=\"information_category_review\" value=\"0\" checked=\"checked\" />
                      ";
            // line 280
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        } else {
            // line 282
            echo "                      <input type=\"radio\" name=\"information_category_review\" value=\"0\" />
                      ";
            // line 283
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                      ";
        }
        // line 284
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 289
        echo (isset($context["text_view"]) ? $context["text_view"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-view\"><span data-toggle=\"tooltip\" title=\"";
        // line 291
        echo (isset($context["help_category_view"]) ? $context["help_category_view"] : null);
        echo "\">";
        echo (isset($context["entry_information_category_view"]) ? $context["entry_information_category_view"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"btn-group\" data-toggle=\"buttons\" id=\"input-information-category-view\">
                      <label class=\"btn btn-default ";
        // line 294
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "block")) {
            echo "active";
        }
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_block"]) ? $context["button_block"] : null);
        echo "\">
                        <input type=\"radio\" name=\"information_category_view\" value=\"block\" autocomplete=\"off\" ";
        // line 295
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "block")) {
            echo "checked";
        }
        echo "><i class=\"fa fa-align-justify\"></i>
                      </label>
                      <label class=\"btn btn-default ";
        // line 297
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "list")) {
            echo "active";
        }
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_list"]) ? $context["button_list"] : null);
        echo "\">
                        <input type=\"radio\" name=\"information_category_view\" value=\"list\" autocomplete=\"off\"";
        // line 298
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "list")) {
            echo "checked";
        }
        echo "><i class=\"fa fa-th-list\"></i>
                      </label>
                      <label class=\"btn btn-default ";
        // line 300
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "grid")) {
            echo "active";
        }
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_grid"]) ? $context["button_grid"] : null);
        echo "\">
                        <input type=\"radio\" name=\"information_category_view\" value=\"grid\" autocomplete=\"off\"";
        // line 301
        if (((isset($context["information_category_view"]) ? $context["information_category_view"] : null) == "grid")) {
            echo "checked";
        }
        echo "><i class=\"fa fa-th\"></i>
                      </label>
                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-view-show\">";
        // line 307
        echo (isset($context["entry_information_category_view_show"]) ? $context["entry_information_category_view_show"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_category_view_show\" id=\"input-information-category-view-show\" class=\"form-control\">
                      ";
        // line 310
        if ((isset($context["information_category_view_show"]) ? $context["information_category_view_show"] : null)) {
            // line 311
            echo "                      <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\">";
            // line 312
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        } else {
            // line 314
            echo "                      <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\" selected=\"selected\">";
            // line 315
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        }
        // line 317
        echo "                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 322
        echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-sort\"><span data-toggle=\"tooltip\" title=\"";
        // line 324
        echo (isset($context["help_category_sort"]) ? $context["help_category_sort"] : null);
        echo "\">";
        echo (isset($context["entry_information_category_sort"]) ? $context["entry_information_category_sort"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_category_sort\" id=\"input-information-category-sort\" class=\"form-control\">
                      ";
        // line 327
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sorts"]) ? $context["sorts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["sort"]) {
            // line 328
            echo "                      ";
            if (($this->getAttribute($context["sort"], "value", array()) == (isset($context["information_category_sort"]) ? $context["information_category_sort"] : null))) {
                // line 329
                echo "                      <option value=\"";
                echo $this->getAttribute($context["sort"], "value", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["sort"], "text", array());
                echo "</option>
                      ";
            } else {
                // line 331
                echo "                      <option value=\"";
                echo $this->getAttribute($context["sort"], "value", array());
                echo "\">";
                echo $this->getAttribute($context["sort"], "text", array());
                echo "</option>
                      ";
            }
            // line 333
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sort'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 334
        echo "                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-sort-show\">";
        // line 338
        echo (isset($context["entry_information_category_sort_show"]) ? $context["entry_information_category_sort_show"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_category_sort_show\" id=\"input-information-category-sort-show\" class=\"form-control\">
                      ";
        // line 341
        if ((isset($context["information_category_sort_show"]) ? $context["information_category_sort_show"] : null)) {
            // line 342
            echo "                      <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\">";
            // line 343
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        } else {
            // line 345
            echo "                      <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\" selected=\"selected\">";
            // line 346
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        }
        // line 348
        echo "                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 353
        echo (isset($context["text_limit"]) ? $context["text_limit"] : null);
        echo "</legend>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-catalog-limit\"><span data-toggle=\"tooltip\" title=\"";
        // line 355
        echo (isset($context["help_information_limit"]) ? $context["help_information_limit"] : null);
        echo "\">";
        echo (isset($context["entry_information_limit"]) ? $context["entry_information_limit"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"information_limit\" value=\"";
        // line 357
        echo (isset($context["information_limit"]) ? $context["information_limit"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_information_limit"]) ? $context["entry_information_limit"] : null);
        echo "\" id=\"input-catalog-limit\" class=\"form-control\" />
                    ";
        // line 358
        if ((isset($context["error_information_limit"]) ? $context["error_information_limit"] : null)) {
            // line 359
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_information_limit"]) ? $context["error_information_limit"] : null);
            echo "</div>
                    ";
        }
        // line 361
        echo "                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-information-category-limit-show\">";
        // line 364
        echo (isset($context["entry_information_category_limit_show"]) ? $context["entry_information_category_limit_show"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"information_category_limit_show\" id=\"input-information-category-limit-show\" class=\"form-control\">
                      ";
        // line 367
        if ((isset($context["information_category_limit_show"]) ? $context["information_category_limit_show"] : null)) {
            // line 368
            echo "                      <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\">";
            // line 369
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        } else {
            // line 371
            echo "                      <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                      <option value=\"0\" selected=\"selected\">";
            // line 372
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                      ";
        }
        // line 374
        echo "                    </select>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-exclusion\">
              <fieldset>
                <legend>";
        // line 381
        echo (isset($context["text_exclusion"]) ? $context["text_exclusion"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-information\"><span data-toggle=\"tooltip\" title=\"";
        // line 383
        echo (isset($context["help_exclusion_information"]) ? $context["help_exclusion_information"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_information"]) ? $context["entry_exclusion_information"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_informations\" value=\"\" placeholder=\"";
        // line 385
        echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
        echo "\" id=\"input-exclusion-information\" class=\"form-control\" />
                    <div id=\"information-exclusion\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 387
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_informations"]) ? $context["exclusion_informations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_information"]) {
            // line 388
            echo "                      <div id=\"information-exclusion";
            echo $this->getAttribute($context["exclusion_information"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 389
            echo $this->getAttribute($context["exclusion_information"], "title", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion[]\" value=\"";
            // line 390
            echo $this->getAttribute($context["exclusion_information"], "information_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 393
        echo "                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 398
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-information-author\"><span data-toggle=\"tooltip\" title=\"";
        // line 400
        echo (isset($context["help_exclusion_information"]) ? $context["help_exclusion_information"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_information_author"]) ? $context["entry_exclusion_information_author"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_informations_author\" value=\"\" placeholder=\"";
        // line 402
        echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
        echo "\" id=\"input-exclusion-information-author\" class=\"form-control\" />
                    <div id=\"information-exclusion-information-author\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 404
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_informations_author"]) ? $context["exclusion_informations_author"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_information_author"]) {
            // line 405
            echo "                      <div id=\"information-exclusion-information-author";
            echo $this->getAttribute($context["exclusion_information_author"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 406
            echo $this->getAttribute($context["exclusion_information_author"], "title", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_information_author[]\" value=\"";
            // line 407
            echo $this->getAttribute($context["exclusion_information_author"], "information_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_information_author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 410
        echo "                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-information-date\"><span data-toggle=\"tooltip\" title=\"";
        // line 414
        echo (isset($context["help_exclusion_information"]) ? $context["help_exclusion_information"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_information_date"]) ? $context["entry_exclusion_information_date"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_informations_date\" value=\"\" placeholder=\"";
        // line 416
        echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
        echo "\" id=\"input-exclusion-information-date\" class=\"form-control\" />
                    <div id=\"information-exclusion-information-date\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 418
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_informations_date"]) ? $context["exclusion_informations_date"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_information_date"]) {
            // line 419
            echo "                      <div id=\"information-exclusion-information-date";
            echo $this->getAttribute($context["exclusion_information_date"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 420
            echo $this->getAttribute($context["exclusion_information_date"], "title", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_information_date[]\" value=\"";
            // line 421
            echo $this->getAttribute($context["exclusion_information_date"], "information_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_information_date'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 424
        echo "                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-information-review\"><span data-toggle=\"tooltip\" title=\"";
        // line 428
        echo (isset($context["help_exclusion_information"]) ? $context["help_exclusion_information"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_information_review"]) ? $context["entry_exclusion_information_review"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_informations_review\" value=\"\" placeholder=\"";
        // line 430
        echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
        echo "\" id=\"input-exclusion-information-review\" class=\"form-control\" />
                    <div id=\"information-exclusion-information-review\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 432
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_informations_review"]) ? $context["exclusion_informations_review"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_information_review"]) {
            // line 433
            echo "                      <div id=\"information-exclusion-information-review";
            echo $this->getAttribute($context["exclusion_information_review"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 434
            echo $this->getAttribute($context["exclusion_information_review"], "title", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_information_review[]\" value=\"";
            // line 435
            echo $this->getAttribute($context["exclusion_information_review"], "information_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_information_review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 438
        echo "                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-information-manufacturer\"><span data-toggle=\"tooltip\" title=\"";
        // line 442
        echo (isset($context["help_exclusion_information"]) ? $context["help_exclusion_information"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_information_manufacturer"]) ? $context["entry_exclusion_information_manufacturer"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_informations_manufacturer\" value=\"\" placeholder=\"";
        // line 444
        echo (isset($context["entry_information"]) ? $context["entry_information"] : null);
        echo "\" id=\"input-exclusion-information-manufacturer\" class=\"form-control\" />
                    <div id=\"information-exclusion-information-manufacturer\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 446
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_informations_manufacturer"]) ? $context["exclusion_informations_manufacturer"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_information_manufacturer"]) {
            // line 447
            echo "                      <div id=\"information-exclusion-information-manufacturer";
            echo $this->getAttribute($context["exclusion_information_manufacturer"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 448
            echo $this->getAttribute($context["exclusion_information_manufacturer"], "title", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_information_manufacturer[]\" value=\"";
            // line 449
            echo $this->getAttribute($context["exclusion_information_manufacturer"], "information_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_information_manufacturer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 452
        echo "                    </div>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 457
        echo (isset($context["text_category"]) ? $context["text_category"] : null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-category-author\"><span data-toggle=\"tooltip\" title=\"";
        // line 459
        echo (isset($context["help_exclusion_category"]) ? $context["help_exclusion_category"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_category_author"]) ? $context["entry_exclusion_category_author"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_categories_author\" value=\"\" placeholder=\"";
        // line 461
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "\" id=\"input-exclusion-category-author\" class=\"form-control\" />
                    <div id=\"information-exclusion-category-author\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 463
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_categories_author"]) ? $context["exclusion_categories_author"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_category_author"]) {
            // line 464
            echo "                      <div id=\"information-exclusion-category-author";
            echo $this->getAttribute($context["exclusion_category_author"], "category_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 465
            echo $this->getAttribute($context["exclusion_category_author"], "name", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_category_author[]\" value=\"";
            // line 466
            echo $this->getAttribute($context["exclusion_category_author"], "category_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_category_author'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 469
        echo "                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-category-date\"><span data-toggle=\"tooltip\" title=\"";
        // line 473
        echo (isset($context["help_exclusion_category"]) ? $context["help_exclusion_category"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_category_date"]) ? $context["entry_exclusion_category_date"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_categories_date\" value=\"\" placeholder=\"";
        // line 475
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "\" id=\"input-exclusion-category-date\" class=\"form-control\" />
                    <div id=\"information-exclusion-category-date\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 477
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_categories_date"]) ? $context["exclusion_categories_date"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_category_date"]) {
            // line 478
            echo "                      <div id=\"information-exclusion-category-date";
            echo $this->getAttribute($context["exclusion_category_date"], "category_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 479
            echo $this->getAttribute($context["exclusion_category_date"], "name", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_category_date[]\" value=\"";
            // line 480
            echo $this->getAttribute($context["exclusion_category_date"], "category_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_category_date'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 483
        echo "                    </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-exclusion-category-review\"><span data-toggle=\"tooltip\" title=\"";
        // line 487
        echo (isset($context["help_exclusion_category"]) ? $context["help_exclusion_category"] : null);
        echo "\">";
        echo (isset($context["entry_exclusion_category_review"]) ? $context["entry_exclusion_category_review"] : null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"exclusion_categories_review\" value=\"\" placeholder=\"";
        // line 489
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "\" id=\"input-exclusion-category-review\" class=\"form-control\" />
                    <div id=\"information-exclusion-category-review\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                    ";
        // line 491
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["exclusion_categories_review"]) ? $context["exclusion_categories_review"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["exclusion_category_review"]) {
            // line 492
            echo "                      <div id=\"information-exclusion-category-review";
            echo $this->getAttribute($context["exclusion_category_review"], "category_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                        ";
            // line 493
            echo $this->getAttribute($context["exclusion_category_review"], "name", array());
            echo "
                        <input type=\"hidden\" name=\"information_exclusion_category_review[]\" value=\"";
            // line 494
            echo $this->getAttribute($context["exclusion_category_review"], "category_id", array());
            echo "\" />
                      </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['exclusion_category_review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 497
        echo "                    </div>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-image\">
              <fieldset>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-category-width\">";
        // line 505
        echo (isset($context["entry_image_category"]) ? $context["entry_image_category"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_width\" value=\"";
        // line 509
        echo (isset($context["information_image_category_width"]) ? $context["information_image_category_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-category-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_height\" value=\"";
        // line 512
        echo (isset($context["information_image_category_height"]) ? $context["information_image_category_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 515
        if ((isset($context["error_image_category"]) ? $context["error_image_category"] : null)) {
            // line 516
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_category"]) ? $context["error_image_category"] : null);
            echo "</div>
                    ";
        }
        // line 518
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-thumb-width\">";
        // line 521
        echo (isset($context["entry_image_thumb"]) ? $context["entry_image_thumb"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_thumb_width\" value=\"";
        // line 525
        echo (isset($context["information_image_thumb_width"]) ? $context["information_image_thumb_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-thumb-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_thumb_height\" value=\"";
        // line 528
        echo (isset($context["information_image_thumb_height"]) ? $context["information_image_thumb_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 531
        if ((isset($context["error_image_thumb"]) ? $context["error_image_thumb"] : null)) {
            // line 532
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_thumb"]) ? $context["error_image_thumb"] : null);
            echo "</div>
                    ";
        }
        // line 534
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-popup-width\">";
        // line 537
        echo (isset($context["entry_image_popup"]) ? $context["entry_image_popup"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_popup_width\" value=\"";
        // line 541
        echo (isset($context["information_image_popup_width"]) ? $context["information_image_popup_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-popup-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_popup_height\" value=\"";
        // line 544
        echo (isset($context["information_image_popup_height"]) ? $context["information_image_popup_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 547
        if ((isset($context["error_image_popup"]) ? $context["error_image_popup"] : null)) {
            // line 548
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_popup"]) ? $context["error_image_popup"] : null);
            echo "</div>
                    ";
        }
        // line 550
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-information-width\">";
        // line 553
        echo (isset($context["entry_image_information"]) ? $context["entry_image_information"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_information_width\" value=\"";
        // line 557
        echo (isset($context["information_image_information_width"]) ? $context["information_image_information_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-information-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_information_height\" value=\"";
        // line 560
        echo (isset($context["information_image_information_height"]) ? $context["information_image_information_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 563
        if ((isset($context["error_image_information"]) ? $context["error_image_information"] : null)) {
            // line 564
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_information"]) ? $context["error_image_information"] : null);
            echo "</div>
                    ";
        }
        // line 566
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-additional-width\">";
        // line 569
        echo (isset($context["entry_image_additional"]) ? $context["entry_image_additional"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_additional_width\" value=\"";
        // line 573
        echo (isset($context["information_image_additional_width"]) ? $context["information_image_additional_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-additional-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_additional_height\" value=\"";
        // line 576
        echo (isset($context["information_image_additional_height"]) ? $context["information_image_additional_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 579
        if ((isset($context["error_image_additional"]) ? $context["error_image_additional"] : null)) {
            // line 580
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_additional"]) ? $context["error_image_additional"] : null);
            echo "</div>
                    ";
        }
        // line 582
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-related\">";
        // line 585
        echo (isset($context["entry_image_related"]) ? $context["entry_image_related"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_related_width\" value=\"";
        // line 589
        echo (isset($context["information_image_related_width"]) ? $context["information_image_related_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-related\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_related_height\" value=\"";
        // line 592
        echo (isset($context["information_image_related_height"]) ? $context["information_image_related_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 595
        if ((isset($context["error_image_related"]) ? $context["error_image_related"] : null)) {
            // line 596
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_related"]) ? $context["error_image_related"] : null);
            echo "</div>
                    ";
        }
        // line 598
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-category-popup-width\">";
        // line 601
        echo (isset($context["entry_image_category_popup"]) ? $context["entry_image_category_popup"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_popup_width\" value=\"";
        // line 605
        echo (isset($context["information_image_category_popup_width"]) ? $context["information_image_category_popup_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-category-popup-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_popup_height\" value=\"";
        // line 608
        echo (isset($context["information_image_category_popup_height"]) ? $context["information_image_category_popup_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 611
        if ((isset($context["error_image_category_popup"]) ? $context["error_image_category_popup"] : null)) {
            // line 612
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_category_popup"]) ? $context["error_image_category_popup"] : null);
            echo "</div>
                    ";
        }
        // line 614
        echo "                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-image-category-additional-width\">";
        // line 617
        echo (isset($context["entry_image_category_additional"]) ? $context["entry_image_category_additional"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"row\">
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_additional_width\" value=\"";
        // line 621
        echo (isset($context["information_image_category_additional_width"]) ? $context["information_image_category_additional_width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-image-category-additional-width\" class=\"form-control\" />
                      </div>
                      <div class=\"col-sm-6\">
                        <input type=\"text\" name=\"information_image_category_additional_height\" value=\"";
        // line 624
        echo (isset($context["information_image_category_additional_height"]) ? $context["information_image_category_additional_height"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" class=\"form-control\" />
                      </div>
                    </div>
                    ";
        // line 627
        if ((isset($context["error_image_category_additional"]) ? $context["error_image_category_additional"] : null)) {
            // line 628
            echo "                    <div class=\"text-danger\">";
            echo (isset($context["error_image_category_additional"]) ? $context["error_image_category_additional"] : null);
            echo "</div>
                    ";
        }
        // line 630
        echo "                  </div>
                </div>
              </fieldset>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
// Information
\$('input[name^=\\'exclusion_informations\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/information/autocomplete&user_token=";
        // line 644
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['title'],
\t\t\t\t\t\tvalue: item['information_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$(this).val('');

\t\t\$('#' + \$(this).next().next().attr('id') + item['value']).remove();

\t\t\$(this).next().next().append('<div id=\"' + \$(this).next().next().attr('id') + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"' + \$(this).next().next().attr('id').replace(/-/g, '_') + '[]\" value=\"' + item['value'] + '\" /></div>');
\t}
});

// Category
\$('input[name^=\\'exclusion_categories\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/category/autocomplete&user_token=";
        // line 669
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_information=1&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['category_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$(this).val('');

\t\t\$('#' + \$(this).next().next().attr('id') + item['value']).remove();

\t\t\$(this).next().next().append('<div id=\"' + \$(this).next().next().attr('id') + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"' + \$(this).next().next().attr('id').replace(/-/g, '_') + '[]\" value=\"' + item['value'] + '\" /></div>');
\t}
});

\$('.well').delegate('.fa-minus-circle', 'click', function() {
\t\$(this).parent().remove();
});
//--></script> 
</div>
";
        // line 695
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/information/setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1717 => 695,  1688 => 669,  1660 => 644,  1644 => 630,  1638 => 628,  1636 => 627,  1628 => 624,  1620 => 621,  1613 => 617,  1608 => 614,  1602 => 612,  1600 => 611,  1592 => 608,  1584 => 605,  1577 => 601,  1572 => 598,  1566 => 596,  1564 => 595,  1556 => 592,  1548 => 589,  1541 => 585,  1536 => 582,  1530 => 580,  1528 => 579,  1520 => 576,  1512 => 573,  1505 => 569,  1500 => 566,  1494 => 564,  1492 => 563,  1484 => 560,  1476 => 557,  1469 => 553,  1464 => 550,  1458 => 548,  1456 => 547,  1448 => 544,  1440 => 541,  1433 => 537,  1428 => 534,  1422 => 532,  1420 => 531,  1412 => 528,  1404 => 525,  1397 => 521,  1392 => 518,  1386 => 516,  1384 => 515,  1376 => 512,  1368 => 509,  1361 => 505,  1351 => 497,  1342 => 494,  1338 => 493,  1333 => 492,  1329 => 491,  1324 => 489,  1317 => 487,  1311 => 483,  1302 => 480,  1298 => 479,  1293 => 478,  1289 => 477,  1284 => 475,  1277 => 473,  1271 => 469,  1262 => 466,  1258 => 465,  1253 => 464,  1249 => 463,  1244 => 461,  1237 => 459,  1232 => 457,  1225 => 452,  1216 => 449,  1212 => 448,  1207 => 447,  1203 => 446,  1198 => 444,  1191 => 442,  1185 => 438,  1176 => 435,  1172 => 434,  1167 => 433,  1163 => 432,  1158 => 430,  1151 => 428,  1145 => 424,  1136 => 421,  1132 => 420,  1127 => 419,  1123 => 418,  1118 => 416,  1111 => 414,  1105 => 410,  1096 => 407,  1092 => 406,  1087 => 405,  1083 => 404,  1078 => 402,  1071 => 400,  1066 => 398,  1059 => 393,  1050 => 390,  1046 => 389,  1041 => 388,  1037 => 387,  1032 => 385,  1025 => 383,  1020 => 381,  1011 => 374,  1006 => 372,  1001 => 371,  996 => 369,  991 => 368,  989 => 367,  983 => 364,  978 => 361,  972 => 359,  970 => 358,  964 => 357,  957 => 355,  952 => 353,  945 => 348,  940 => 346,  935 => 345,  930 => 343,  925 => 342,  923 => 341,  917 => 338,  911 => 334,  905 => 333,  897 => 331,  889 => 329,  886 => 328,  882 => 327,  874 => 324,  869 => 322,  862 => 317,  857 => 315,  852 => 314,  847 => 312,  842 => 311,  840 => 310,  834 => 307,  823 => 301,  815 => 300,  808 => 298,  800 => 297,  793 => 295,  785 => 294,  777 => 291,  772 => 289,  765 => 284,  760 => 283,  757 => 282,  752 => 280,  749 => 279,  747 => 278,  744 => 277,  739 => 276,  736 => 275,  731 => 273,  728 => 272,  726 => 271,  721 => 269,  715 => 265,  710 => 264,  707 => 263,  702 => 261,  699 => 260,  697 => 259,  694 => 258,  689 => 257,  686 => 256,  681 => 254,  678 => 253,  676 => 252,  671 => 250,  665 => 246,  660 => 245,  657 => 244,  652 => 242,  649 => 241,  647 => 240,  644 => 239,  639 => 238,  636 => 237,  631 => 235,  628 => 234,  626 => 233,  621 => 231,  615 => 227,  610 => 226,  607 => 225,  602 => 223,  599 => 222,  597 => 221,  594 => 220,  589 => 219,  586 => 218,  581 => 216,  578 => 215,  576 => 214,  569 => 212,  564 => 209,  558 => 207,  556 => 206,  550 => 205,  543 => 203,  537 => 199,  532 => 197,  527 => 196,  522 => 194,  517 => 193,  515 => 192,  509 => 189,  504 => 187,  495 => 180,  489 => 179,  481 => 177,  473 => 175,  470 => 174,  466 => 173,  462 => 172,  454 => 169,  448 => 165,  443 => 164,  440 => 163,  435 => 161,  432 => 160,  430 => 159,  427 => 158,  422 => 157,  419 => 156,  414 => 154,  411 => 153,  409 => 152,  402 => 150,  396 => 146,  391 => 145,  388 => 144,  383 => 142,  380 => 141,  378 => 140,  375 => 139,  370 => 138,  367 => 137,  362 => 135,  359 => 134,  357 => 133,  350 => 131,  345 => 129,  338 => 124,  333 => 123,  330 => 122,  325 => 120,  322 => 119,  320 => 118,  317 => 117,  312 => 116,  309 => 115,  304 => 113,  301 => 112,  299 => 111,  294 => 109,  288 => 105,  283 => 104,  280 => 103,  275 => 101,  272 => 100,  270 => 99,  267 => 98,  262 => 97,  259 => 96,  254 => 94,  251 => 93,  249 => 92,  244 => 90,  238 => 86,  233 => 85,  230 => 84,  225 => 82,  222 => 81,  220 => 80,  217 => 79,  212 => 78,  209 => 77,  204 => 75,  201 => 74,  199 => 73,  194 => 71,  188 => 67,  183 => 66,  180 => 65,  175 => 63,  172 => 62,  170 => 61,  167 => 60,  162 => 59,  159 => 58,  154 => 56,  151 => 55,  149 => 54,  144 => 52,  138 => 48,  133 => 46,  128 => 45,  123 => 43,  118 => 42,  116 => 41,  110 => 38,  101 => 32,  97 => 31,  93 => 30,  89 => 29,  84 => 27,  78 => 24,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-theme" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-theme" class="form-horizontal">*/
/*           <ul class="nav nav-tabs">*/
/*             <li class="active"><a href="#tab-general" data-toggle="tab">{{ tab_general }}</a></li>*/
/*             <li><a href="#tab-category" data-toggle="tab">{{ tab_category }}</a></li>*/
/*             <li><a href="#tab-exclusion" data-toggle="tab">{{ tab_exclusion }}</a></li>*/
/*             <li><a href="#tab-image" data-toggle="tab">{{ tab_image }}</a></li>*/
/*           </ul>*/
/*           <div class="tab-content">*/
/*             <div class="tab-pane active" id="tab-general">*/
/*               <fieldset>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_setting_status" id="input-status" class="form-control">*/
/*                       {% if information_setting_status %}*/
/*                       <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                       <option value="0">{{ text_disabled }}</option>*/
/*                       {% else %}*/
/*                       <option value="1">{{ text_enabled }}</option>*/
/*                       <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                       {% endif %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_information_author }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_information_author %}*/
/*                       <input type="radio" name="information_information_author" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_author" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_information_author %}*/
/*                       <input type="radio" name="information_information_author" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_author" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_information_date }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_information_date %}*/
/*                       <input type="radio" name="information_information_date" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_date" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_information_date %}*/
/*                       <input type="radio" name="information_information_date" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_date" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_information_review }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_information_review %}*/
/*                       <input type="radio" name="information_information_review" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_review" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_information_review %}*/
/*                       <input type="radio" name="information_information_review" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_review" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_information_manufacturer }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_information_manufacturer %}*/
/*                       <input type="radio" name="information_information_manufacturer" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_manufacturer" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_information_manufacturer %}*/
/*                       <input type="radio" name="information_information_manufacturer" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_information_manufacturer" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_review }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_review }}">{{ entry_review }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_review_status %}*/
/*                       <input type="radio" name="information_review_status" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_review_status" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_review_status %}*/
/*                       <input type="radio" name="information_review_status" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_review_status" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_review_guest }}">{{ entry_review_guest }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_review_guest %}*/
/*                       <input type="radio" name="information_review_guest" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_review_guest" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_review_guest %}*/
/*                       <input type="radio" name="information_review_guest" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_review_guest" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_captcha }}">{{ entry_captcha }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_captcha" id="input-captcha" class="form-control">*/
/*                       <option value="">{{ text_none }}</option>*/
/*                       {% for captcha in captchas %}*/
/*                       {% if captcha.value == information_captcha %}*/
/*                       <option value="{{ captcha.value }}" selected="selected">{{ captcha.text }}</option>*/
/*                       {% else %}*/
/*                       <option value="{{ captcha.value }}">{{ captcha.text }}</option>*/
/*                       {% endif %}*/
/*                       {% endfor %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-category">*/
/*               <fieldset>*/
/*                 <legend>{{ text_information }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-show">{{ entry_information_category_show }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_category_show" id="input-information-category-show" class="form-control">*/
/*                       {% if information_category_show %}*/
/*                       <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                       <option value="0">{{ text_disabled }}</option>*/
/*                       {% else %}*/
/*                       <option value="1">{{ text_enabled }}</option>*/
/*                       <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                       {% endif %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-description-limit"><span data-toggle="tooltip" title="{{ help_information_description_length }}">{{ entry_information_description_length }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="information_description_length" value="{{ information_description_length }}" placeholder="{{ entry_information_description_length }}" id="input-description-limit" class="form-control" />*/
/*                     {% if error_information_description_length %}*/
/*                     <div class="text-danger">{{ error_information_description_length }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="{{ help_information_count }}">{{ entry_information_count }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_count %}*/
/*                       <input type="radio" name="information_count" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_count" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_count %}*/
/*                       <input type="radio" name="information_count" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_count" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_category_author }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_category_author %}*/
/*                       <input type="radio" name="information_category_author" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_author" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_category_author %}*/
/*                       <input type="radio" name="information_category_author" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_author" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_category_date }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_category_date %}*/
/*                       <input type="radio" name="information_category_date" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_date" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_category_date %}*/
/*                       <input type="radio" name="information_category_date" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_date" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ entry_information_category_review }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <label class="radio-inline"> {% if information_category_review %}*/
/*                       <input type="radio" name="information_category_review" value="1" checked="checked" />*/
/*                       {{ text_yes }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_review" value="1" />*/
/*                       {{ text_yes }}*/
/*                       {% endif %} </label>*/
/*                     <label class="radio-inline"> {% if not information_category_review %}*/
/*                       <input type="radio" name="information_category_review" value="0" checked="checked" />*/
/*                       {{ text_no }}*/
/*                       {% else %}*/
/*                       <input type="radio" name="information_category_review" value="0" />*/
/*                       {{ text_no }}*/
/*                       {% endif %} </label>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_view }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-view"><span data-toggle="tooltip" title="{{ help_category_view }}">{{ entry_information_category_view }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="btn-group" data-toggle="buttons" id="input-information-category-view">*/
/*                       <label class="btn btn-default {% if information_category_view == 'block' %}active{% endif %}" data-toggle="tooltip" title="{{ button_block }}">*/
/*                         <input type="radio" name="information_category_view" value="block" autocomplete="off" {% if information_category_view == 'block' %}checked{% endif %}><i class="fa fa-align-justify"></i>*/
/*                       </label>*/
/*                       <label class="btn btn-default {% if information_category_view == 'list' %}active{% endif %}" data-toggle="tooltip" title="{{ button_list }}">*/
/*                         <input type="radio" name="information_category_view" value="list" autocomplete="off"{% if information_category_view == 'list' %}checked{% endif %}><i class="fa fa-th-list"></i>*/
/*                       </label>*/
/*                       <label class="btn btn-default {% if information_category_view == 'grid' %}active{% endif %}" data-toggle="tooltip" title="{{ button_grid }}">*/
/*                         <input type="radio" name="information_category_view" value="grid" autocomplete="off"{% if information_category_view == 'grid' %}checked{% endif %}><i class="fa fa-th"></i>*/
/*                       </label>*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-view-show">{{ entry_information_category_view_show }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_category_view_show" id="input-information-category-view-show" class="form-control">*/
/*                       {% if information_category_view_show %}*/
/*                       <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                       <option value="0">{{ text_disabled }}</option>*/
/*                       {% else %}*/
/*                       <option value="1">{{ text_enabled }}</option>*/
/*                       <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                       {% endif %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_sort }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-sort"><span data-toggle="tooltip" title="{{ help_category_sort }}">{{ entry_information_category_sort }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_category_sort" id="input-information-category-sort" class="form-control">*/
/*                       {% for sort in sorts %}*/
/*                       {% if sort.value == information_category_sort %}*/
/*                       <option value="{{ sort.value }}" selected="selected">{{ sort.text }}</option>*/
/*                       {% else %}*/
/*                       <option value="{{ sort.value }}">{{ sort.text }}</option>*/
/*                       {% endif %}*/
/*                       {% endfor %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-sort-show">{{ entry_information_category_sort_show }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_category_sort_show" id="input-information-category-sort-show" class="form-control">*/
/*                       {% if information_category_sort_show %}*/
/*                       <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                       <option value="0">{{ text_disabled }}</option>*/
/*                       {% else %}*/
/*                       <option value="1">{{ text_enabled }}</option>*/
/*                       <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                       {% endif %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_limit }}</legend>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-catalog-limit"><span data-toggle="tooltip" title="{{ help_information_limit }}">{{ entry_information_limit }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="information_limit" value="{{ information_limit }}" placeholder="{{ entry_information_limit }}" id="input-catalog-limit" class="form-control" />*/
/*                     {% if error_information_limit %}*/
/*                     <div class="text-danger">{{ error_information_limit }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-information-category-limit-show">{{ entry_information_category_limit_show }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="information_category_limit_show" id="input-information-category-limit-show" class="form-control">*/
/*                       {% if information_category_limit_show %}*/
/*                       <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                       <option value="0">{{ text_disabled }}</option>*/
/*                       {% else %}*/
/*                       <option value="1">{{ text_enabled }}</option>*/
/*                       <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                       {% endif %}*/
/*                     </select>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-exclusion">*/
/*               <fieldset>*/
/*                 <legend>{{ text_exclusion }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-information"><span data-toggle="tooltip" title="{{ help_exclusion_information }}">{{ entry_exclusion_information }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_informations" value="" placeholder="{{ entry_information }}" id="input-exclusion-information" class="form-control" />*/
/*                     <div id="information-exclusion" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_information in exclusion_informations %}*/
/*                       <div id="information-exclusion{{ exclusion_information.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_information.title }}*/
/*                         <input type="hidden" name="information_exclusion[]" value="{{ exclusion_information.information_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_information }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-information-author"><span data-toggle="tooltip" title="{{ help_exclusion_information }}">{{ entry_exclusion_information_author }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_informations_author" value="" placeholder="{{ entry_information }}" id="input-exclusion-information-author" class="form-control" />*/
/*                     <div id="information-exclusion-information-author" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_information_author in exclusion_informations_author %}*/
/*                       <div id="information-exclusion-information-author{{ exclusion_information_author.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_information_author.title }}*/
/*                         <input type="hidden" name="information_exclusion_information_author[]" value="{{ exclusion_information_author.information_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-information-date"><span data-toggle="tooltip" title="{{ help_exclusion_information }}">{{ entry_exclusion_information_date }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_informations_date" value="" placeholder="{{ entry_information }}" id="input-exclusion-information-date" class="form-control" />*/
/*                     <div id="information-exclusion-information-date" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_information_date in exclusion_informations_date %}*/
/*                       <div id="information-exclusion-information-date{{ exclusion_information_date.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_information_date.title }}*/
/*                         <input type="hidden" name="information_exclusion_information_date[]" value="{{ exclusion_information_date.information_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-information-review"><span data-toggle="tooltip" title="{{ help_exclusion_information }}">{{ entry_exclusion_information_review }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_informations_review" value="" placeholder="{{ entry_information }}" id="input-exclusion-information-review" class="form-control" />*/
/*                     <div id="information-exclusion-information-review" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_information_review in exclusion_informations_review %}*/
/*                       <div id="information-exclusion-information-review{{ exclusion_information_review.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_information_review.title }}*/
/*                         <input type="hidden" name="information_exclusion_information_review[]" value="{{ exclusion_information_review.information_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-information-manufacturer"><span data-toggle="tooltip" title="{{ help_exclusion_information }}">{{ entry_exclusion_information_manufacturer }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_informations_manufacturer" value="" placeholder="{{ entry_information }}" id="input-exclusion-information-manufacturer" class="form-control" />*/
/*                     <div id="information-exclusion-information-manufacturer" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_information_manufacturer in exclusion_informations_manufacturer %}*/
/*                       <div id="information-exclusion-information-manufacturer{{ exclusion_information_manufacturer.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_information_manufacturer.title }}*/
/*                         <input type="hidden" name="information_exclusion_information_manufacturer[]" value="{{ exclusion_information_manufacturer.information_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*               <fieldset>*/
/*                 <legend>{{ text_category }}</legend>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-category-author"><span data-toggle="tooltip" title="{{ help_exclusion_category }}">{{ entry_exclusion_category_author }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_categories_author" value="" placeholder="{{ entry_category }}" id="input-exclusion-category-author" class="form-control" />*/
/*                     <div id="information-exclusion-category-author" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_category_author in exclusion_categories_author %}*/
/*                       <div id="information-exclusion-category-author{{ exclusion_category_author.category_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_category_author.name }}*/
/*                         <input type="hidden" name="information_exclusion_category_author[]" value="{{ exclusion_category_author.category_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-category-date"><span data-toggle="tooltip" title="{{ help_exclusion_category }}">{{ entry_exclusion_category_date }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_categories_date" value="" placeholder="{{ entry_category }}" id="input-exclusion-category-date" class="form-control" />*/
/*                     <div id="information-exclusion-category-date" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_category_date in exclusion_categories_date %}*/
/*                       <div id="information-exclusion-category-date{{ exclusion_category_date.category_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_category_date.name }}*/
/*                         <input type="hidden" name="information_exclusion_category_date[]" value="{{ exclusion_category_date.category_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label" for="input-exclusion-category-review"><span data-toggle="tooltip" title="{{ help_exclusion_category }}">{{ entry_exclusion_category_review }}</span></label>*/
/*                   <div class="col-sm-10">*/
/*                     <input type="text" name="exclusion_categories_review" value="" placeholder="{{ entry_category }}" id="input-exclusion-category-review" class="form-control" />*/
/*                     <div id="information-exclusion-category-review" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                     {% for exclusion_category_review in exclusion_categories_review %}*/
/*                       <div id="information-exclusion-category-review{{ exclusion_category_review.category_id }}"><i class="fa fa-minus-circle"></i> */
/*                         {{ exclusion_category_review.name }}*/
/*                         <input type="hidden" name="information_exclusion_category_review[]" value="{{ exclusion_category_review.category_id }}" />*/
/*                       </div>*/
/*                     {% endfor %}*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-image">*/
/*               <fieldset>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-category-width">{{ entry_image_category }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_width" value="{{ information_image_category_width }}" placeholder="{{ entry_width }}" id="input-image-category-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_height" value="{{ information_image_category_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_category %}*/
/*                     <div class="text-danger">{{ error_image_category }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-thumb-width">{{ entry_image_thumb }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_thumb_width" value="{{ information_image_thumb_width }}" placeholder="{{ entry_width }}" id="input-image-thumb-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_thumb_height" value="{{ information_image_thumb_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_thumb %}*/
/*                     <div class="text-danger">{{ error_image_thumb }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-popup-width">{{ entry_image_popup }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_popup_width" value="{{ information_image_popup_width }}" placeholder="{{ entry_width }}" id="input-image-popup-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_popup_height" value="{{ information_image_popup_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_popup %}*/
/*                     <div class="text-danger">{{ error_image_popup }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-information-width">{{ entry_image_information }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_information_width" value="{{ information_image_information_width }}" placeholder="{{ entry_width }}" id="input-image-information-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_information_height" value="{{ information_image_information_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_information %}*/
/*                     <div class="text-danger">{{ error_image_information }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-additional-width">{{ entry_image_additional }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_additional_width" value="{{ information_image_additional_width }}" placeholder="{{ entry_width }}" id="input-image-additional-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_additional_height" value="{{ information_image_additional_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_additional %}*/
/*                     <div class="text-danger">{{ error_image_additional }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-related">{{ entry_image_related }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_related_width" value="{{ information_image_related_width }}" placeholder="{{ entry_width }}" id="input-image-related" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_related_height" value="{{ information_image_related_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_related %}*/
/*                     <div class="text-danger">{{ error_image_related }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-category-popup-width">{{ entry_image_category_popup }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_popup_width" value="{{ information_image_category_popup_width }}" placeholder="{{ entry_width }}" id="input-image-category-popup-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_popup_height" value="{{ information_image_category_popup_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_category_popup %}*/
/*                     <div class="text-danger">{{ error_image_category_popup }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label" for="input-image-category-additional-width">{{ entry_image_category_additional }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <div class="row">*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_additional_width" value="{{ information_image_category_additional_width }}" placeholder="{{ entry_width }}" id="input-image-category-additional-width" class="form-control" />*/
/*                       </div>*/
/*                       <div class="col-sm-6">*/
/*                         <input type="text" name="information_image_category_additional_height" value="{{ information_image_category_additional_height }}" placeholder="{{ entry_height }}" class="form-control" />*/
/*                       </div>*/
/*                     </div>*/
/*                     {% if error_image_category_additional %}*/
/*                     <div class="text-danger">{{ error_image_category_additional }}</div>*/
/*                     {% endif %}*/
/*                   </div>*/
/*                 </div>*/
/*               </fieldset>*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <script type="text/javascript"><!--*/
/* // Information*/
/* $('input[name^=\'exclusion_informations\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/information/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['title'],*/
/* 						value: item['information_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$(this).val('');*/
/* */
/* 		$('#' + $(this).next().next().attr('id') + item['value']).remove();*/
/* */
/* 		$(this).next().next().append('<div id="' + $(this).next().next().attr('id') + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="' + $(this).next().next().attr('id').replace(/-/g, '_') + '[]" value="' + item['value'] + '" /></div>');*/
/* 	}*/
/* });*/
/* */
/* // Category*/
/* $('input[name^=\'exclusion_categories\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/category/autocomplete&user_token={{ user_token }}&filter_information=1&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['category_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$(this).val('');*/
/* */
/* 		$('#' + $(this).next().next().attr('id') + item['value']).remove();*/
/* */
/* 		$(this).next().next().append('<div id="' + $(this).next().next().attr('id') + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="' + $(this).next().next().attr('id').replace(/-/g, '_') + '[]" value="' + item['value'] + '" /></div>');*/
/* 	}*/
/* });*/
/* */
/* $('.well').delegate('.fa-minus-circle', 'click', function() {*/
/* 	$(this).parent().remove();*/
/* });*/
/* //--></script> */
/* </div>*/
/* {{ footer }}*/
