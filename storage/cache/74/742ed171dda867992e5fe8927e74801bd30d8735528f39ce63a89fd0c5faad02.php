<?php

/* catalog/information_form.twig */
class __TwigTemplate_0376638b89e11b16b7c39c68e661fa4f07fb54300ab631f91ceee93a2247340e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-information\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">";
        // line 16
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 17
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 21
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 23
        echo (isset($context["text_form"]) ? $context["text_form"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 26
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-information\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 28
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
            <li><a href=\"#tab-data\" data-toggle=\"tab\">";
        // line 29
        echo (isset($context["tab_data"]) ? $context["tab_data"] : null);
        echo "</a></li>

            <li><a href=\"#tab-links\" data-toggle=\"tab\">";
        // line 31
        echo (isset($context["tab_links"]) ? $context["tab_links"] : null);
        echo "</a></li>
            <li><a href=\"#tab-attribute\" data-toggle=\"tab\">";
        // line 32
        echo (isset($context["tab_attribute"]) ? $context["tab_attribute"] : null);
        echo "</a></li>
            <li><a href=\"#tab-image\" data-toggle=\"tab\">";
        // line 33
        echo (isset($context["tab_image"]) ? $context["tab_image"] : null);
        echo "</a></li>
        
            <li><a href=\"#tab-seo\" data-toggle=\"tab\">";
        // line 35
        echo (isset($context["tab_seo"]) ? $context["tab_seo"] : null);
        echo "</a></li>
            <li><a href=\"#tab-design\" data-toggle=\"tab\">";
        // line 36
        echo (isset($context["tab_design"]) ? $context["tab_design"] : null);
        echo "</a></li>
          </ul>
          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <ul class=\"nav nav-tabs\" id=\"language\">
                ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 42
            echo "                <li><a href=\"#language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "              </ul>
              <div class=\"tab-content\">";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 46
            echo "                <div class=\"tab-pane\" id=\"language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-title";
            // line 48
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_title"]) ? $context["entry_title"] : null);
            echo "</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"information_description[";
            // line 50
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][title]\" value=\"";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
            echo "\" placeholder=\"";
            echo (isset($context["entry_title"]) ? $context["entry_title"] : null);
            echo "\" id=\"input-title";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\" />
                      ";
            // line 51
            if ($this->getAttribute((isset($context["error_title"]) ? $context["error_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) {
                // line 52
                echo "                      <div class=\"text-danger\">";
                echo $this->getAttribute((isset($context["error_title"]) ? $context["error_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array");
                echo "</div>
                      ";
            }
            // line 53
            echo " </div>
                  </div>

                  <div class=\"form-group\">
                    <label class=\"col-sm-2 control-label\" for=\"input-header";
            // line 57
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\"><span data-toggle=\"tooltip\" title=\"";
            echo (isset($context["help_header"]) ? $context["help_header"] : null);
            echo "\">";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo "</span></label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"information_description[";
            // line 59
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][header]\" value=\"";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "header", array())) : (""));
            echo "\" placeholder=\"";
            echo (isset($context["entry_header"]) ? $context["entry_header"] : null);
            echo "\" id=\"input-header";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\" />
                    </div>
                  </div>
                  <div class=\"form-group\">
                    <label class=\"col-sm-2 control-label\" for=\"input-short-description";
            // line 63
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_short_description"]) ? $context["entry_short_description"] : null);
            echo "<br /><button type=\"button\" class=\"btn btn-primary btn-xs summernote\" data-toggle=\"button\" aria-pressed=\"false\" autocomplete=\"off\" data-language=\"";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Summernote</button></label>
                    <div class=\"col-sm-10\">
                      <textarea name=\"information_description[";
            // line 65
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][short_description]\" placeholder=\"";
            echo (isset($context["entry_short_description"]) ? $context["entry_short_description"] : null);
            echo "\" id=\"input-short-description";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\">";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "short_description", array())) : (""));
            echo "</textarea>
                     </div>
                  </div>
        
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-description";
            // line 70
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_description"]) ? $context["entry_description"] : null);
            echo "</label>
                    <div class=\"col-sm-10\">
                      <textarea name=\"information_description[";
            // line 72
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][description]\" placeholder=\"";
            echo (isset($context["entry_description"]) ? $context["entry_description"] : null);
            echo "\" id=\"input-description";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"summernote\" data-lang=\"";
            echo (isset($context["summernote"]) ? $context["summernote"] : null);
            echo "\" class=\"form-control\">";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "description", array())) : (""));
            echo "</textarea>
                      ";
            // line 73
            if ($this->getAttribute((isset($context["error_description"]) ? $context["error_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) {
                // line 74
                echo "                      <div class=\"text-danger\">";
                echo $this->getAttribute((isset($context["error_description"]) ? $context["error_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array");
                echo "</div>
                      ";
            }
            // line 75
            echo " </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-meta-title";
            // line 78
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_meta_title"]) ? $context["entry_meta_title"] : null);
            echo "</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"information_description[";
            // line 80
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][meta_title]\" value=\"";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "meta_title", array())) : (""));
            echo "\" placeholder=\"";
            echo (isset($context["entry_meta_title"]) ? $context["entry_meta_title"] : null);
            echo "\" id=\"input-meta-title";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\" />
                      ";
            // line 81
            if ($this->getAttribute((isset($context["error_meta_title"]) ? $context["error_meta_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) {
                // line 82
                echo "                      <div class=\"text-danger\">";
                echo $this->getAttribute((isset($context["error_meta_title"]) ? $context["error_meta_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array");
                echo "</div>
                      ";
            }
            // line 83
            echo " </div>
                  </div>
                  <div class=\"form-group\">
                    <label class=\"col-sm-2 control-label\" for=\"input-meta-description";
            // line 86
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_meta_description"]) ? $context["entry_meta_description"] : null);
            echo "</label>
                    <div class=\"col-sm-10\">
                      <textarea name=\"information_description[";
            // line 88
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][meta_description]\" rows=\"5\" placeholder=\"";
            echo (isset($context["entry_meta_description"]) ? $context["entry_meta_description"] : null);
            echo "\" id=\"input-meta-description";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\">";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "meta_description", array())) : (""));
            echo "</textarea>
                    </div>
                  </div>
                  <div class=\"form-group\">
                    <label class=\"col-sm-2 control-label\" for=\"input-meta-keyword";
            // line 92
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (isset($context["entry_meta_keyword"]) ? $context["entry_meta_keyword"] : null);
            echo "</label>
                    <div class=\"col-sm-10\">
                      <textarea name=\"information_description[";
            // line 94
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][meta_keyword]\" rows=\"5\" placeholder=\"";
            echo (isset($context["entry_meta_keyword"]) ? $context["entry_meta_keyword"] : null);
            echo "\" id=\"input-meta-keyword";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\">";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "meta_keyword", array())) : (""));
            echo "</textarea>

                    </div>
                  </div>
                  <div class=\"form-group\">
                    <label class=\"col-sm-2 control-label\" for=\"input-tag";
            // line 99
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\"><span data-toggle=\"tooltip\" title=\"";
            echo (isset($context["help_tag"]) ? $context["help_tag"] : null);
            echo "\">";
            echo (isset($context["entry_tag"]) ? $context["entry_tag"] : null);
            echo "</span></label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"information_description[";
            // line 101
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][tag]\" value=\"";
            echo (($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["information_description"]) ? $context["information_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "tag", array())) : (""));
            echo "\" placeholder=\"";
            echo (isset($context["entry_tag"]) ? $context["entry_tag"] : null);
            echo "\" id=\"input-tag";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"form-control\" />
        
                    </div>
                  </div>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "</div>
            </div>
            <div class=\"tab-pane\" id=\"tab-data\">

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date-added\">";
        // line 111
        echo (isset($context["entry_date_added"]) ? $context["entry_date_added"] : null);
        echo "</label>
                <div class=\"col-sm-3\">
                  <div class=\"input-group datetime\">
                    <input type=\"text\" name=\"date_added\" value=\"";
        // line 114
        echo (isset($context["date_added"]) ? $context["date_added"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_date_added"]) ? $context["entry_date_added"] : null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-added\" class=\"form-control\" />
                    <span class=\"input-group-btn\">
                    <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                    </span></div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date-available\">";
        // line 121
        echo (isset($context["entry_date_available"]) ? $context["entry_date_available"] : null);
        echo "</label>
                <div class=\"col-sm-3\">
                  <div class=\"input-group datetime\">
                    <input type=\"text\" name=\"date_available\" value=\"";
        // line 124
        echo (isset($context["date_available"]) ? $context["date_available"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_date_available"]) ? $context["entry_date_available"] : null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-available\" class=\"form-control\" />
                    <span class=\"input-group-btn\">
                    <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                    </span></div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-date-end\">";
        // line 131
        echo (isset($context["entry_date_end"]) ? $context["entry_date_end"] : null);
        echo "</label>
                <div class=\"col-sm-3\">
                  <div class=\"input-group datetime\">
                    <input type=\"text\" name=\"date_end\" value=\"";
        // line 134
        echo (isset($context["date_end"]) ? $context["date_end"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_date_end"]) ? $context["entry_date_end"] : null);
        echo "\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"input-date-end\" class=\"form-control\" />
                    <span class=\"input-group-btn\">
                    <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                    </span></div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-user\">";
        // line 141
        echo (isset($context["entry_user"]) ? $context["entry_user"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"user_id\" id=\"input-user\" class=\"form-control\">
                    ";
        // line 144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["user_groups"]) ? $context["user_groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user_group"]) {
            // line 145
            echo "                    <optgroup label=\"";
            echo $this->getAttribute($context["user_group"], "name", array());
            echo "\">
                    ";
            // line 146
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["user_group"], "users", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 147
                echo "                    ";
                if (($this->getAttribute($context["user"], "user_id", array()) == (isset($context["user_id"]) ? $context["user_id"] : null))) {
                    // line 148
                    echo "                    <option value=\"";
                    echo $this->getAttribute($context["user"], "user_id", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["user"], "username", array());
                    echo "</option>
                    ";
                } else {
                    // line 150
                    echo "                    <option value=\"";
                    echo $this->getAttribute($context["user"], "user_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["user"], "username", array());
                    echo "</option>
                    ";
                }
                // line 152
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 153
            echo "                    </optgroup>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "                  </select>
                </div>
              </div>
        
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">";
        // line 160
        echo (isset($context["entry_store"]) ? $context["entry_store"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 163
            echo "                    <div class=\"checkbox\">
                      <label> ";
            // line 164
            if (twig_in_filter($this->getAttribute($context["store"], "store_id", array()), (isset($context["information_store"]) ? $context["information_store"] : null))) {
                // line 165
                echo "                        <input type=\"checkbox\" name=\"information_store[]\" value=\"";
                echo $this->getAttribute($context["store"], "store_id", array());
                echo "\" checked=\"checked\" />
                        ";
                // line 166
                echo $this->getAttribute($context["store"], "name", array());
                echo "
                        ";
            } else {
                // line 168
                echo "                        <input type=\"checkbox\" name=\"information_store[]\" value=\"";
                echo $this->getAttribute($context["store"], "store_id", array());
                echo "\" />
                        ";
                // line 169
                echo $this->getAttribute($context["store"], "name", array());
                echo "
                        ";
            }
            // line 170
            echo "</label>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 172
        echo "</div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-bottom\"><span data-toggle=\"tooltip\" title=\"";
        // line 176
        echo (isset($context["help_bottom"]) ? $context["help_bottom"] : null);
        echo "\">";
        echo (isset($context["entry_bottom"]) ? $context["entry_bottom"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <div class=\"checkbox\">
                    <label>";
        // line 179
        if ((isset($context["bottom"]) ? $context["bottom"] : null)) {
            // line 180
            echo "                      <input type=\"checkbox\" name=\"bottom\" value=\"1\" checked=\"checked\" id=\"input-bottom\" />
                      ";
        } else {
            // line 182
            echo "                      <input type=\"checkbox\" name=\"bottom\" value=\"1\" id=\"input-bottom\" />
                      ";
        }
        // line 184
        echo "                      &nbsp;</label>
                  </div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 189
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"status\" id=\"input-status\" class=\"form-control\">
                    ";
        // line 192
        if ((isset($context["status"]) ? $context["status"] : null)) {
            // line 193
            echo "                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                    <option value=\"0\">";
            // line 194
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                    ";
        } else {
            // line 196
            echo "                    <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                    <option value=\"0\" selected=\"selected\">";
            // line 197
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                    ";
        }
        // line 199
        echo "                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 203
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"sort_order\" value=\"";
        // line 205
        echo (isset($context["sort_order"]) ? $context["sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input-sort-order\" class=\"form-control\" />
                </div>
              </div>
            </div>

            <div class=\"tab-pane\" id=\"tab-links\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-manufacturer\"><span data-toggle=\"tooltip\" title=\"";
        // line 212
        echo (isset($context["help_manufacturer"]) ? $context["help_manufacturer"] : null);
        echo "\">";
        echo (isset($context["entry_manufacturer"]) ? $context["entry_manufacturer"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"manufacturer\" value=\"";
        // line 214
        echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_manufacturer"]) ? $context["entry_manufacturer"] : null);
        echo "\" id=\"input-manufacturer\" class=\"form-control\" />
                  <input type=\"hidden\" name=\"manufacturer_id\" value=\"";
        // line 215
        echo (isset($context["manufacturer_id"]) ? $context["manufacturer_id"] : null);
        echo "\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-category\"><span data-toggle=\"tooltip\" title=\"";
        // line 219
        echo (isset($context["help_category"]) ? $context["help_category"] : null);
        echo "\">";
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"category\" value=\"\" placeholder=\"";
        // line 221
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "\" id=\"input-category\" class=\"form-control\" />
                  <div id=\"information-category\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 222
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["information_categories"]) ? $context["information_categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information_category"]) {
            // line 223
            echo "                    ";
            if (((isset($context["main_category"]) ? $context["main_category"] : null) && ((isset($context["main_category"]) ? $context["main_category"] : null) == $this->getAttribute($context["information_category"], "category_id", array())))) {
                // line 224
                echo "                    <div id=\"information-category";
                echo $this->getAttribute($context["information_category"], "category_id", array());
                echo "\" class=\"text-success\"><i class=\"fa fa-minus-circle\"></i> <i class=\"fa fa-dot-circle-o\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["help_main_category"]) ? $context["help_main_category"] : null);
                echo "\"></i> ";
                echo $this->getAttribute($context["information_category"], "name", array());
                echo "
                      <input type=\"hidden\" name=\"information_category[]\" value=\"";
                // line 225
                echo $this->getAttribute($context["information_category"], "category_id", array());
                echo "\" />
                      <input type=\"hidden\" name=\"main_category\" value=\"";
                // line 226
                echo $this->getAttribute($context["information_category"], "category_id", array());
                echo "\" />
                    </div>
                    ";
            } else {
                // line 229
                echo "                    <div id=\"information-category";
                echo $this->getAttribute($context["information_category"], "category_id", array());
                echo "\"><i class=\"fa fa-minus-circle\"></i> <i class=\"fa fa-circle-o\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
                echo "\"></i> ";
                echo $this->getAttribute($context["information_category"], "name", array());
                echo "
                      <input type=\"hidden\" name=\"information_category[]\" value=\"";
                // line 230
                echo $this->getAttribute($context["information_category"], "category_id", array());
                echo "\" />
                    </div>
                    ";
            }
            // line 233
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information_category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-filter\"><span data-toggle=\"tooltip\" title=\"";
        // line 237
        echo (isset($context["help_filter"]) ? $context["help_filter"] : null);
        echo "\">";
        echo (isset($context["entry_filter"]) ? $context["entry_filter"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"filter\" value=\"\" placeholder=\"";
        // line 239
        echo (isset($context["entry_filter"]) ? $context["entry_filter"] : null);
        echo "\" id=\"input-filter\" class=\"form-control\" />
                  <div id=\"information-filter\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 240
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["information_filters"]) ? $context["information_filters"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information_filter"]) {
            // line 241
            echo "                    <div id=\"information-filter";
            echo $this->getAttribute($context["information_filter"], "filter_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> ";
            echo $this->getAttribute($context["information_filter"], "name", array());
            echo "
                      <input type=\"hidden\" name=\"information_filter[]\" value=\"";
            // line 242
            echo $this->getAttribute($context["information_filter"], "filter_id", array());
            echo "\" />
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information_filter'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 244
        echo "</div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-related-product\"><span data-toggle=\"tooltip\" title=\"";
        // line 248
        echo (isset($context["help_related"]) ? $context["help_related"] : null);
        echo "\">";
        echo (isset($context["entry_related_product"]) ? $context["entry_related_product"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"related_product\" value=\"\" placeholder=\"";
        // line 250
        echo (isset($context["entry_related_product"]) ? $context["entry_related_product"] : null);
        echo "\" id=\"input-related-product\" class=\"form-control\" />
                  <div id=\"product-related\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 251
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_relateds"]) ? $context["product_relateds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product_related"]) {
            // line 252
            echo "                    <div id=\"product-related";
            echo $this->getAttribute($context["product_related"], "product_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                      ";
            // line 253
            if (($this->getAttribute($context["product_related"], "route", array()) && ($this->getAttribute($context["product_related"], "route", array()) > 0))) {
                // line 254
                echo "                      <i class=\"fa fa-long-arrow-right\"></i> 
                      ";
            } elseif (($this->getAttribute(            // line 255
$context["product_related"], "route", array()) && ($this->getAttribute($context["product_related"], "route", array()) < 0))) {
                // line 256
                echo "                      <i class=\"fa fa-long-arrow-left\"></i> 
                      ";
            } else {
                // line 258
                echo "                      <i class=\"fa fa-exchange\"></i> 
                      ";
            }
            // line 260
            echo "                      ";
            echo $this->getAttribute($context["product_related"], "name", array());
            echo "
                      <input type=\"hidden\" name=\"product_related[";
            // line 261
            echo $this->getAttribute($context["product_related"], "product_id", array());
            echo "][product_id]\" value=\"";
            echo $this->getAttribute($context["product_related"], "product_id", array());
            echo "\" />
                      <input type=\"hidden\" name=\"product_related[";
            // line 262
            echo $this->getAttribute($context["product_related"], "product_id", array());
            echo "][route]\" value=\"";
            echo $this->getAttribute($context["product_related"], "route", array());
            echo "\" />
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_related'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 264
        echo "</div>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-related-information\"><span data-toggle=\"tooltip\" title=\"";
        // line 268
        echo (isset($context["help_related"]) ? $context["help_related"] : null);
        echo "\">";
        echo (isset($context["entry_related_information"]) ? $context["entry_related_information"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"related_information\" value=\"\" placeholder=\"";
        // line 270
        echo (isset($context["entry_related_information"]) ? $context["entry_related_information"] : null);
        echo "\" id=\"input-related-information\" class=\"form-control\" />
                  <div id=\"information-related\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 271
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["information_relateds"]) ? $context["information_relateds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information_related"]) {
            // line 272
            echo "                    <div id=\"information-related";
            echo $this->getAttribute($context["information_related"], "information_id", array());
            echo "\"><i class=\"fa fa-minus-circle\"></i> 
                      ";
            // line 273
            if (($this->getAttribute($context["information_related"], "route", array()) && ($this->getAttribute($context["information_related"], "route", array()) > 0))) {
                // line 274
                echo "                      <i class=\"fa fa-long-arrow-right\"></i> 
                      ";
            } elseif (($this->getAttribute(            // line 275
$context["information_related"], "route", array()) && ($this->getAttribute($context["information_related"], "route", array()) < 0))) {
                // line 276
                echo "                      <i class=\"fa fa-long-arrow-left\"></i> 
                      ";
            } else {
                // line 278
                echo "                      <i class=\"fa fa-exchange\"></i> 
                      ";
            }
            // line 280
            echo "                      ";
            echo $this->getAttribute($context["information_related"], "title", array());
            echo "
                      <input type=\"hidden\" name=\"information_related[";
            // line 281
            echo $this->getAttribute($context["information_related"], "information_id", array());
            echo "][information_id]\" value=\"";
            echo $this->getAttribute($context["information_related"], "information_id", array());
            echo "\" />
                      <input type=\"hidden\" name=\"information_related[";
            // line 282
            echo $this->getAttribute($context["information_related"], "information_id", array());
            echo "][route]\" value=\"";
            echo $this->getAttribute($context["information_related"], "route", array());
            echo "\" />
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information_related'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 284
        echo "</div>
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-attribute\">
              <div class=\"table-responsive\">
                <table id=\"attribute\" class=\"table table-striped table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-left\">";
        // line 293
        echo (isset($context["entry_attribute"]) ? $context["entry_attribute"] : null);
        echo "</td>
                      <td class=\"text-left\">";
        // line 294
        echo (isset($context["entry_text"]) ? $context["entry_text"] : null);
        echo "</td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 299
        $context["attribute_row"] = 0;
        // line 300
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["information_attributes"]) ? $context["information_attributes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information_attribute"]) {
            // line 301
            echo "                    <tr id=\"attribute-row";
            echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
            echo "\">
                      <td class=\"text-left\" style=\"width: 40%;\"><input type=\"text\" name=\"information_attribute[";
            // line 302
            echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
            echo "][name]\" value=\"";
            echo $this->getAttribute($context["information_attribute"], "name", array());
            echo "\" placeholder=\"";
            echo (isset($context["entry_attribute"]) ? $context["entry_attribute"] : null);
            echo "\" class=\"form-control\" />
                      <input type=\"hidden\" name=\"information_attribute[";
            // line 303
            echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
            echo "][attribute_id]\" value=\"";
            echo $this->getAttribute($context["information_attribute"], "attribute_id", array());
            echo "\" /></td>
                      <td class=\"text-left\">";
            // line 304
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 305
                echo "                        <div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                          <textarea name=\"information_attribute[";
                // line 306
                echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
                echo "][information_attribute_description][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][text]\" rows=\"5\" placeholder=\"";
                echo (isset($context["entry_text"]) ? $context["entry_text"] : null);
                echo "\" class=\"form-control\">";
                echo (($this->getAttribute($this->getAttribute($context["information_attribute"], "information_attribute_description", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($this->getAttribute($context["information_attribute"], "information_attribute_description", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "text", array())) : (""));
                echo "</textarea>
                        </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 308
            echo "</td>
                      <td class=\"text-right\"><button type=\"button\" onclick=\"\$('#attribute-row";
            // line 309
            echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
            echo "').remove();\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>
                    </tr>
                    ";
            // line 311
            $context["attribute_row"] = ((isset($context["attribute_row"]) ? $context["attribute_row"] : null) + 1);
            // line 312
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information_attribute'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 313
        echo "                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan=\"2\"></td>
                      <td class=\"text-right\"><button type=\"button\" onclick=\"addAttribute();\" data-toggle=\"tooltip\" title=\"";
        // line 317
        echo (isset($context["button_attribute_add"]) ? $context["button_attribute_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-image\">
              <div class=\"table-responsive\">
                <table class=\"table table-striped table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-left\">";
        // line 328
        echo (isset($context["entry_image"]) ? $context["entry_image"] : null);
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class=\"text-left\"><a href=\"\" id=\"thumb-image\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 333
        echo (isset($context["thumb"]) ? $context["thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                        <input type=\"hidden\" name=\"image\" value=\"";
        // line 334
        echo (isset($context["image"]) ? $context["image"] : null);
        echo "\" id=\"input-image\" /></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class=\"table-responsive\">
                <table id=\"images\" class=\"table table-striped table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-left\">";
        // line 343
        echo (isset($context["entry_additional_image"]) ? $context["entry_additional_image"] : null);
        echo "</td>
                      <td class=\"text-right\">";
        // line 344
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 349
        $context["image_row"] = 0;
        // line 350
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["information_images"]) ? $context["information_images"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information_image"]) {
            // line 351
            echo "                    <tr id=\"image-row";
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "\">
                      <td class=\"text-left\"><a href=\"\" id=\"thumb-image";
            // line 352
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
            echo $this->getAttribute($context["information_image"], "thumb", array());
            echo "\" alt=\"\" title=\"\" data-placeholder=\"";
            echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
            echo "\" /></a>
                        <input type=\"hidden\" name=\"information_image[";
            // line 353
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "][image]\" value=\"";
            echo $this->getAttribute($context["information_image"], "image", array());
            echo "\" id=\"input-image";
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "\" /></td>
                      <td class=\"text-right\"><input type=\"text\" name=\"information_image[";
            // line 354
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "][sort_order]\" value=\"";
            echo $this->getAttribute($context["information_image"], "sort_order", array());
            echo "\" placeholder=\"";
            echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
            echo "\" class=\"form-control\" /></td>
                      <td class=\"text-left\"><button type=\"button\" onclick=\"\$('#image-row";
            // line 355
            echo (isset($context["image_row"]) ? $context["image_row"] : null);
            echo "').remove();\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>
                    </tr>
                    ";
            // line 357
            $context["image_row"] = ((isset($context["image_row"]) ? $context["image_row"] : null) + 1);
            // line 358
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information_image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 359
        echo "                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan=\"2\"></td>
                      <td class=\"text-left\"><button type=\"button\" onclick=\"addImage();\" data-toggle=\"tooltip\" title=\"";
        // line 363
        echo (isset($context["button_image_add"]) ? $context["button_image_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
        
            <div class=\"tab-pane\" id=\"tab-seo\">
              <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
        // line 371
        echo (isset($context["text_keyword"]) ? $context["text_keyword"] : null);
        echo "</div>
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-left\">";
        // line 376
        echo (isset($context["entry_store"]) ? $context["entry_store"] : null);
        echo "</td>
                      <td class=\"text-left\">";
        // line 377
        echo (isset($context["entry_keyword"]) ? $context["entry_keyword"] : null);
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                  ";
        // line 381
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 382
            echo "                  <tr>
                    <td class=\"text-left\">";
            // line 383
            echo $this->getAttribute($context["store"], "name", array());
            echo "</td>
                    <td class=\"text-left\">";
            // line 384
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 385
                echo "                      <div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                        <input type=\"text\" name=\"information_seo_url[";
                // line 386
                echo $this->getAttribute($context["store"], "store_id", array());
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "]\" value=\"";
                if ($this->getAttribute($this->getAttribute((isset($context["information_seo_url"]) ? $context["information_seo_url"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) {
                    echo $this->getAttribute($this->getAttribute((isset($context["information_seo_url"]) ? $context["information_seo_url"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array");
                }
                echo "\" placeholder=\"";
                echo (isset($context["entry_keyword"]) ? $context["entry_keyword"] : null);
                echo "\" class=\"form-control\" />
                      </div>
                      ";
                // line 388
                if ($this->getAttribute($this->getAttribute((isset($context["error_keyword"]) ? $context["error_keyword"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) {
                    // line 389
                    echo "                      <div class=\"text-danger\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["error_keyword"]) ? $context["error_keyword"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array");
                    echo "</div>
                      ";
                }
                // line 390
                echo " 
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 391
            echo "</td>
                  </tr>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 394
        echo "                  </tbody>
                </table>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-design\">
              <div class=\"table-responsive\">
                <table class=\"table table-bordered table-hover\">
                  <thead>
                    <tr>
                      <td class=\"text-left\">";
        // line 403
        echo (isset($context["entry_store"]) ? $context["entry_store"] : null);
        echo "</td>
                      <td class=\"text-left\">";
        // line 404
        echo (isset($context["entry_layout"]) ? $context["entry_layout"] : null);
        echo "</td>
                    </tr>
                  </thead>
                  <tbody>
                  
                  ";
        // line 409
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 410
            echo "                  <tr>
                    <td class=\"text-left\">";
            // line 411
            echo $this->getAttribute($context["store"], "name", array());
            echo "</td>
                    <td class=\"text-left\"><select name=\"information_layout[";
            // line 412
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" class=\"form-control\">
                        <option value=\"\"></option>
                        ";
            // line 414
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["layouts"]) ? $context["layouts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
                // line 415
                echo "                        ";
                if (($this->getAttribute((isset($context["information_layout"]) ? $context["information_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") && ($this->getAttribute((isset($context["information_layout"]) ? $context["information_layout"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array") == $this->getAttribute($context["layout"], "layout_id", array())))) {
                    // line 416
                    echo "                        <option value=\"";
                    echo $this->getAttribute($context["layout"], "layout_id", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["layout"], "name", array());
                    echo "</option>
                        ";
                } else {
                    // line 418
                    echo "                        <option value=\"";
                    echo $this->getAttribute($context["layout"], "layout_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["layout"], "name", array());
                    echo "</option>
                        ";
                }
                // line 420
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 421
            echo "                      </select></td>
                  </tr>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 424
        echo "                    </tbody>
                  
                </table>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <link href=\"view/javascript/codemirror/lib/codemirror.css\" rel=\"stylesheet\" />
  <link href=\"view/javascript/codemirror/theme/monokai.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/codemirror.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/xml.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/formatting.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote-image-attributes.js\"></script> 
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script> 

  <script type=\"text/javascript\"><!--
// Manufacturer
\$('input[name=\\'manufacturer\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/manufacturer/autocomplete&user_token=";
        // line 449
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tjson.unshift({
\t\t\t\t\tmanufacturer_id: 0,
\t\t\t\t\tname: '";
        // line 454
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "'
\t\t\t\t});

\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['manufacturer_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'manufacturer\\']').val(item['label']);
\t\t\$('input[name=\\'manufacturer_id\\']').val(item['value']);
\t}
});

// Category
\$('input[name=\\'category\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/category/autocomplete&user_token=";
        // line 476
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_information=1&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['category_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'category\\']').val('');

\t\t\$('#information-category' + item['value']).remove();

\t\t\$('#information-category').append('<div id=\"information-category' + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> <i class=\"fa fa-circle-o\" data-toggle=\"tooltip\" title=\"";
        // line 493
        echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
        echo "\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"information_category[]\" value=\"' + item['value'] + '\" /></div>');
\t}
});

\$('#information-category').delegate('.fa-minus-circle', 'click', function() {
\t\$(this).parent().remove();
});

\$('#information-category').delegate('.fa-circle-o', 'click', function() {
\t\$('#information-category div').children('.fa-dot-circle-o').toggleClass('fa-circle-o fa-dot-circle-o');
    \$('#information-category div').removeClass('text-success');
    \$(this).toggleClass('fa-circle-o fa-dot-circle-o');
    \$(this).parent().addClass('text-success');
    \$('input[name=\\'main_category\\']').remove();
    \$(this).parent().append('<input type=\"hidden\" name=\"main_category\" value=\"' + \$(this).parent().find('input[name=\\'information_category[]\\']').val() + '\" />');
\t\$('#information-category div').children('.fa-circle-o').attr({'title':'";
        // line 508
        echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
        echo "', 'data-original-title':'";
        echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
        echo "'});
    \$(this).attr({'title':'";
        // line 509
        echo (isset($context["help_main_category"]) ? $context["help_main_category"] : null);
        echo "', 'data-original-title':'";
        echo (isset($context["help_main_category"]) ? $context["help_main_category"] : null);
        echo "'});
});
\$('#information-category').delegate('.fa-dot-circle-o', 'click', function() {
    \$(this).toggleClass('fa-circle-o fa-dot-circle-o');
    \$('input[name=\\'main_category\\']').remove();
    \$(this).parent().removeClass('text-success');
    \$(this).attr({'title':'";
        // line 515
        echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
        echo "', 'data-original-title':'";
        echo (isset($context["help_main_category_add"]) ? $context["help_main_category_add"] : null);
        echo "'});
});

// Filter
\$('input[name=\\'filter\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/filter/autocomplete&user_token=";
        // line 522
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['filter_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter\\']').val('');

\t\t\$('#information-filter' + item['value']).remove();

\t\t\$('#information-filter').append('<div id=\"information-filter' + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"information_filter[]\" value=\"' + item['value'] + '\" /></div>');
\t}
});

\$('#information-filter').delegate('.fa-minus-circle', 'click', function() {
\t\$(this).parent().remove();
});

// Related Information
\$('input[name=\\'related_information\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/information/autocomplete&user_token=";
        // line 551
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        if ((isset($context["information_id"]) ? $context["information_id"] : null)) {
            echo "&information_id=";
            echo (isset($context["information_id"]) ? $context["information_id"] : null);
        }
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['title'],
\t\t\t\t\t\tvalue: item['information_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'related_information\\']').val('');

\t\t\$('#information-related' + item['value']).remove();

\t\t\$('#information-related').append('<div id=\"information-related' + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> <i class=\"fa fa-exchange\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"information_related[' + item['value'] + '][information_id]\" value=\"' + item['value'] + '\" /><input type=\"hidden\" name=\"information_related[' + item['value'] + '][route]\" value=\"0\" /></div>');
\t}
});

\$('#information-related').delegate('.fa-minus-circle', 'click', function() {
\t\$(this).parent().remove();
});

\$('#information-related').delegate('.fa-exchange', 'click', function() {
    \$(this).toggleClass('fa-exchange fa-long-arrow-right');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('1');
});
\$('#information-related').delegate('.fa-long-arrow-right', 'click', function() {
    \$(this).toggleClass('fa-long-arrow-right fa-long-arrow-left');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('-1');
});
\$('#information-related').delegate('.fa-long-arrow-left', 'click', function() {
    \$(this).toggleClass('fa-long-arrow-left fa-exchange');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('0');
});

// Related Product
\$('input[name=\\'related_product\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/product/autocomplete&user_token=";
        // line 593
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'related_product\\']').val('');

\t\t\$('#product-related' + item['value']).remove();

\t\t\$('#product-related').append('<div id=\"product-related' + item['value'] + '\"><i class=\"fa fa-minus-circle\"></i> <i class=\"fa fa-exchange\"></i> ' + item['label'] + '<input type=\"hidden\" name=\"product_related[' + item['value'] + '][product_id]\" value=\"' + item['value'] + '\" /><input type=\"hidden\" name=\"product_related[' + item['value'] + '][route]\" value=\"0\" /></div>');
\t}
});

\$('#product-related').delegate('.fa-minus-circle', 'click', function() {
\t\$(this).parent().remove();
});

\$('#product-related').delegate('.fa-exchange', 'click', function() {
    \$(this).toggleClass('fa-exchange fa-long-arrow-right');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('1');
});
\$('#product-related').delegate('.fa-long-arrow-right', 'click', function() {
    \$(this).toggleClass('fa-long-arrow-right fa-long-arrow-left');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('-1');
});
\$('#product-related').delegate('.fa-long-arrow-left', 'click', function() {
    \$(this).toggleClass('fa-long-arrow-left fa-exchange');
\t\$(this).parent().find('input[name\$=\\'[route]\\']').val('0');
});
//--></script> 
  <script type=\"text/javascript\"><!--
var attribute_row = ";
        // line 632
        echo (isset($context["attribute_row"]) ? $context["attribute_row"] : null);
        echo ";

function addAttribute() {
    html  = '<tr id=\"attribute-row' + attribute_row + '\">';
\thtml += '  <td class=\"text-left\" style=\"width: 20%;\"><input type=\"text\" name=\"information_attribute[' + attribute_row + '][name]\" value=\"\" placeholder=\"";
        // line 636
        echo (isset($context["entry_attribute"]) ? $context["entry_attribute"] : null);
        echo "\" class=\"form-control\" /><input type=\"hidden\" name=\"information_attribute[' + attribute_row + '][attribute_id]\" value=\"\" /></td>';
\thtml += '  <td class=\"text-left\">';
\t";
        // line 638
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 639
            echo "\thtml += '<div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span><textarea name=\"information_attribute[' + attribute_row + '][information_attribute_description][";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][text]\" rows=\"5\" placeholder=\"";
            echo (isset($context["entry_text"]) ? $context["entry_text"] : null);
            echo "\" class=\"form-control\"></textarea></div>';
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 641
        echo "\thtml += '  </td>';
\thtml += '  <td class=\"text-right\"><button type=\"button\" onclick=\"\$(\\'#attribute-row' + attribute_row + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 642
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>';
    html += '</tr>';

\t\$('#attribute tbody').append(html);

\tattributeautocomplete(attribute_row);

\tattribute_row++;
}

function attributeautocomplete(attribute_row) {
\t\$('input[name=\\'information_attribute[' + attribute_row + '][name]\\']').autocomplete({
\t\t'source': function(request, response) {
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=catalog/attribute/autocomplete&user_token=";
        // line 656
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\t\tdataType: 'json',
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tcategory: item.attribute_group,
\t\t\t\t\t\t\tlabel: item.name,
\t\t\t\t\t\t\tvalue: item.attribute_id
\t\t\t\t\t\t}
\t\t\t\t\t}));
\t\t\t\t}
\t\t\t});
\t\t},
\t\t'select': function(item) {
\t\t\t\$('input[name=\\'information_attribute[' + attribute_row + '][name]\\']').val(item['label']);
\t\t\t\$('input[name=\\'information_attribute[' + attribute_row + '][attribute_id]\\']').val(item['value']);
\t\t}
\t});
}

\$('#attribute tbody tr').each(function(index, element) {
\tattributeautocomplete(index);
});
//--></script> 
  <script type=\"text/javascript\"><!--
var image_row = ";
        // line 681
        echo (isset($context["image_row"]) ? $context["image_row"] : null);
        echo ";

function addImage() {
\thtml  = '<tr id=\"image-row' + image_row + '\">';
\thtml += '  <td class=\"text-left\"><a href=\"\" id=\"thumb-image' + image_row + '\"data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 685
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a><input type=\"hidden\" name=\"information_image[' + image_row + '][image]\" value=\"\" id=\"input-image' + image_row + '\" /></td>';
\thtml += '  <td class=\"text-right\"><input type=\"text\" name=\"information_image[' + image_row + '][sort_order]\" value=\"\" placeholder=\"";
        // line 686
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" class=\"form-control\" /></td>';
\thtml += '  <td class=\"text-left\"><button type=\"button\" onclick=\"\$(\\'#image-row' + image_row  + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 687
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>';
\thtml += '</tr>';

\t\$('#images tbody').append(html);

\timage_row++;
}
//--></script> 
  <script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: '";
        // line 697
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickTime: false
});

\$('.time').datetimepicker({
\tlanguage: '";
        // line 702
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickDate: false
});

\$('.datetime').datetimepicker({
\tlanguage: '";
        // line 707
        echo (isset($context["datepicker"]) ? $context["datepicker"] : null);
        echo "',
\tpickDate: true,
\tpickTime: true
});
//--></script> 
        
  <script type=\"text/javascript\"><!--
\$('#language a:first').tab('show');
//--></script></div>

  <style type=\"text/css\"><!--
#information-category .fa-circle-o {
    display: none;
}
#information-category div:hover .fa-circle-o {
    display: inline;
}
--></style>
  <script type=\"text/javascript\"><!--
\$('.summernote').on('click', function () {
\tif (\$(this).hasClass('active')) {
\t\t\$('#input-short-description' + \$(this).data('language')).summernote('destroy');
\t} else {
\t\t\$('#input-short-description' + \$(this).data('language')).summernote({
\t\t\tfocus: true,
\t\t\tlang: '";
        // line 732
        echo (isset($context["summernote"]) ? $context["summernote"] : null);
        echo "'
\t\t});
\t}
})

\$(document).ready(function() {
\t\$('[name*=\\'[short_description]\\']').each(function() {
\t\tif (\$(this).val()) {
            \$(this).parent().prev().children('.summernote').trigger('click');
\t\t}
\t});
});
//--></script>
        
";
        // line 746
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/information_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1643 => 746,  1626 => 732,  1598 => 707,  1590 => 702,  1582 => 697,  1569 => 687,  1565 => 686,  1559 => 685,  1552 => 681,  1524 => 656,  1507 => 642,  1504 => 641,  1487 => 639,  1483 => 638,  1478 => 636,  1471 => 632,  1429 => 593,  1380 => 551,  1348 => 522,  1336 => 515,  1325 => 509,  1319 => 508,  1301 => 493,  1281 => 476,  1256 => 454,  1248 => 449,  1221 => 424,  1213 => 421,  1207 => 420,  1199 => 418,  1191 => 416,  1188 => 415,  1184 => 414,  1179 => 412,  1175 => 411,  1172 => 410,  1168 => 409,  1160 => 404,  1156 => 403,  1145 => 394,  1137 => 391,  1130 => 390,  1124 => 389,  1122 => 388,  1109 => 386,  1100 => 385,  1096 => 384,  1092 => 383,  1089 => 382,  1085 => 381,  1078 => 377,  1074 => 376,  1066 => 371,  1055 => 363,  1049 => 359,  1043 => 358,  1041 => 357,  1034 => 355,  1026 => 354,  1018 => 353,  1010 => 352,  1005 => 351,  1000 => 350,  998 => 349,  990 => 344,  986 => 343,  974 => 334,  968 => 333,  960 => 328,  946 => 317,  940 => 313,  934 => 312,  932 => 311,  925 => 309,  922 => 308,  907 => 306,  898 => 305,  894 => 304,  888 => 303,  880 => 302,  875 => 301,  870 => 300,  868 => 299,  860 => 294,  856 => 293,  845 => 284,  834 => 282,  828 => 281,  823 => 280,  819 => 278,  815 => 276,  813 => 275,  810 => 274,  808 => 273,  803 => 272,  799 => 271,  795 => 270,  788 => 268,  782 => 264,  771 => 262,  765 => 261,  760 => 260,  756 => 258,  752 => 256,  750 => 255,  747 => 254,  745 => 253,  740 => 252,  736 => 251,  732 => 250,  725 => 248,  719 => 244,  710 => 242,  703 => 241,  699 => 240,  695 => 239,  688 => 237,  677 => 233,  671 => 230,  662 => 229,  656 => 226,  652 => 225,  643 => 224,  640 => 223,  636 => 222,  632 => 221,  625 => 219,  618 => 215,  612 => 214,  605 => 212,  593 => 205,  588 => 203,  582 => 199,  577 => 197,  572 => 196,  567 => 194,  562 => 193,  560 => 192,  554 => 189,  547 => 184,  543 => 182,  539 => 180,  537 => 179,  529 => 176,  523 => 172,  515 => 170,  510 => 169,  505 => 168,  500 => 166,  495 => 165,  493 => 164,  490 => 163,  486 => 162,  481 => 160,  474 => 155,  467 => 153,  461 => 152,  453 => 150,  445 => 148,  442 => 147,  438 => 146,  433 => 145,  429 => 144,  423 => 141,  411 => 134,  405 => 131,  393 => 124,  387 => 121,  375 => 114,  369 => 111,  362 => 106,  344 => 101,  335 => 99,  321 => 94,  314 => 92,  301 => 88,  294 => 86,  289 => 83,  283 => 82,  281 => 81,  271 => 80,  264 => 78,  259 => 75,  253 => 74,  251 => 73,  239 => 72,  232 => 70,  218 => 65,  209 => 63,  196 => 59,  187 => 57,  181 => 53,  175 => 52,  173 => 51,  163 => 50,  156 => 48,  150 => 46,  146 => 45,  143 => 44,  126 => 42,  122 => 41,  114 => 36,  110 => 35,  105 => 33,  101 => 32,  97 => 31,  92 => 29,  88 => 28,  83 => 26,  77 => 23,  73 => 21,  65 => 17,  63 => 16,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-information" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">{% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_form }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal">*/
/*           <ul class="nav nav-tabs">*/
/*             <li class="active"><a href="#tab-general" data-toggle="tab">{{ tab_general }}</a></li>*/
/*             <li><a href="#tab-data" data-toggle="tab">{{ tab_data }}</a></li>*/
/* */
/*             <li><a href="#tab-links" data-toggle="tab">{{ tab_links }}</a></li>*/
/*             <li><a href="#tab-attribute" data-toggle="tab">{{ tab_attribute }}</a></li>*/
/*             <li><a href="#tab-image" data-toggle="tab">{{ tab_image }}</a></li>*/
/*         */
/*             <li><a href="#tab-seo" data-toggle="tab">{{ tab_seo }}</a></li>*/
/*             <li><a href="#tab-design" data-toggle="tab">{{ tab_design }}</a></li>*/
/*           </ul>*/
/*           <div class="tab-content">*/
/*             <div class="tab-pane active" id="tab-general">*/
/*               <ul class="nav nav-tabs" id="language">*/
/*                 {% for language in languages %}*/
/*                 <li><a href="#language{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/*                 {% endfor %}*/
/*               </ul>*/
/*               <div class="tab-content">{% for language in languages %}*/
/*                 <div class="tab-pane" id="language{{ language.language_id }}">*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-title{{ language.language_id }}">{{ entry_title }}</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="information_description[{{ language.language_id }}][title]" value="{{ information_description[language.language_id] ? information_description[language.language_id].title }}" placeholder="{{ entry_title }}" id="input-title{{ language.language_id }}" class="form-control" />*/
/*                       {% if error_title[language.language_id] %}*/
/*                       <div class="text-danger">{{ error_title[language.language_id] }}</div>*/
/*                       {% endif %} </div>*/
/*                   </div>*/
/* */
/*                   <div class="form-group">*/
/*                     <label class="col-sm-2 control-label" for="input-header{{ language.language_id }}"><span data-toggle="tooltip" title="{{ help_header }}">{{ entry_header }}</span></label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="information_description[{{ language.language_id }}][header]" value="{{ information_description[language.language_id] ? information_description[language.language_id].header }}" placeholder="{{ entry_header }}" id="input-header{{ language.language_id }}" class="form-control" />*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group">*/
/*                     <label class="col-sm-2 control-label" for="input-short-description{{ language.language_id }}">{{ entry_short_description }}<br /><button type="button" class="btn btn-primary btn-xs summernote" data-toggle="button" aria-pressed="false" autocomplete="off" data-language="{{ language.language_id }}">Summernote</button></label>*/
/*                     <div class="col-sm-10">*/
/*                       <textarea name="information_description[{{ language.language_id }}][short_description]" placeholder="{{ entry_short_description }}" id="input-short-description{{ language.language_id }}" class="form-control">{{ information_description[language.language_id] ? information_description[language.language_id].short_description }}</textarea>*/
/*                      </div>*/
/*                   </div>*/
/*         */
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-description{{ language.language_id }}">{{ entry_description }}</label>*/
/*                     <div class="col-sm-10">*/
/*                       <textarea name="information_description[{{ language.language_id }}][description]" placeholder="{{ entry_description }}" id="input-description{{ language.language_id }}" data-toggle="summernote" data-lang="{{ summernote }}" class="form-control">{{ information_description[language.language_id] ? information_description[language.language_id].description }}</textarea>*/
/*                       {% if error_description[language.language_id] %}*/
/*                       <div class="text-danger">{{ error_description[language.language_id] }}</div>*/
/*                       {% endif %} </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-meta-title{{ language.language_id }}">{{ entry_meta_title }}</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="information_description[{{ language.language_id }}][meta_title]" value="{{ information_description[language.language_id] ? information_description[language.language_id].meta_title }}" placeholder="{{ entry_meta_title }}" id="input-meta-title{{ language.language_id }}" class="form-control" />*/
/*                       {% if error_meta_title[language.language_id] %}*/
/*                       <div class="text-danger">{{ error_meta_title[language.language_id] }}</div>*/
/*                       {% endif %} </div>*/
/*                   </div>*/
/*                   <div class="form-group">*/
/*                     <label class="col-sm-2 control-label" for="input-meta-description{{ language.language_id }}">{{ entry_meta_description }}</label>*/
/*                     <div class="col-sm-10">*/
/*                       <textarea name="information_description[{{ language.language_id }}][meta_description]" rows="5" placeholder="{{ entry_meta_description }}" id="input-meta-description{{ language.language_id }}" class="form-control">{{ information_description[language.language_id] ? information_description[language.language_id].meta_description }}</textarea>*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group">*/
/*                     <label class="col-sm-2 control-label" for="input-meta-keyword{{ language.language_id }}">{{ entry_meta_keyword }}</label>*/
/*                     <div class="col-sm-10">*/
/*                       <textarea name="information_description[{{ language.language_id }}][meta_keyword]" rows="5" placeholder="{{ entry_meta_keyword }}" id="input-meta-keyword{{ language.language_id }}" class="form-control">{{ information_description[language.language_id] ? information_description[language.language_id].meta_keyword }}</textarea>*/
/* */
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group">*/
/*                     <label class="col-sm-2 control-label" for="input-tag{{ language.language_id }}"><span data-toggle="tooltip" title="{{ help_tag }}">{{ entry_tag }}</span></label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="information_description[{{ language.language_id }}][tag]" value="{{ information_description[language.language_id] ? information_description[language.language_id].tag }}" placeholder="{{ entry_tag }}" id="input-tag{{ language.language_id }}" class="form-control" />*/
/*         */
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*                 {% endfor %}</div>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-data">*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-date-added">{{ entry_date_added }}</label>*/
/*                 <div class="col-sm-3">*/
/*                   <div class="input-group datetime">*/
/*                     <input type="text" name="date_added" value="{{ date_added }}" placeholder="{{ entry_date_added }}" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-date-added" class="form-control" />*/
/*                     <span class="input-group-btn">*/
/*                     <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                     </span></div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-date-available">{{ entry_date_available }}</label>*/
/*                 <div class="col-sm-3">*/
/*                   <div class="input-group datetime">*/
/*                     <input type="text" name="date_available" value="{{ date_available }}" placeholder="{{ entry_date_available }}" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-date-available" class="form-control" />*/
/*                     <span class="input-group-btn">*/
/*                     <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                     </span></div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-date-end">{{ entry_date_end }}</label>*/
/*                 <div class="col-sm-3">*/
/*                   <div class="input-group datetime">*/
/*                     <input type="text" name="date_end" value="{{ date_end }}" placeholder="{{ entry_date_end }}" data-date-format="YYYY-MM-DD HH:mm:ss" id="input-date-end" class="form-control" />*/
/*                     <span class="input-group-btn">*/
/*                     <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                     </span></div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-user">{{ entry_user }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="user_id" id="input-user" class="form-control">*/
/*                     {% for user_group in user_groups %}*/
/*                     <optgroup label="{{ user_group.name }}">*/
/*                     {% for user in user_group.users %}*/
/*                     {% if user.user_id == user_id %}*/
/*                     <option value="{{ user.user_id }}" selected="selected">{{ user.username }}</option>*/
/*                     {% else %}*/
/*                     <option value="{{ user.user_id }}">{{ user.username }}</option>*/
/*                     {% endif %}*/
/*                     {% endfor %}*/
/*                     </optgroup>*/
/*                     {% endfor %}*/
/*                   </select>*/
/*                 </div>*/
/*               </div>*/
/*         */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label">{{ entry_store }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <div class="well well-sm" style="height: 150px; overflow: auto;"> {% for store in stores %}*/
/*                     <div class="checkbox">*/
/*                       <label> {% if store.store_id in information_store %}*/
/*                         <input type="checkbox" name="information_store[]" value="{{ store.store_id }}" checked="checked" />*/
/*                         {{ store.name }}*/
/*                         {% else %}*/
/*                         <input type="checkbox" name="information_store[]" value="{{ store.store_id }}" />*/
/*                         {{ store.name }}*/
/*                         {% endif %}</label>*/
/*                     </div>*/
/*                     {% endfor %}</div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-bottom"><span data-toggle="tooltip" title="{{ help_bottom }}">{{ entry_bottom }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <div class="checkbox">*/
/*                     <label>{% if bottom %}*/
/*                       <input type="checkbox" name="bottom" value="1" checked="checked" id="input-bottom" />*/
/*                       {% else %}*/
/*                       <input type="checkbox" name="bottom" value="1" id="input-bottom" />*/
/*                       {% endif %}*/
/*                       &nbsp;</label>*/
/*                   </div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="status" id="input-status" class="form-control">*/
/*                     {% if status %}*/
/*                     <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                     <option value="0">{{ text_disabled }}</option>*/
/*                     {% else %}*/
/*                     <option value="1">{{ text_enabled }}</option>*/
/*                     <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                     {% endif %}*/
/*                   </select>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-sort-order">{{ entry_sort_order }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="sort_order" value="{{ sort_order }}" placeholder="{{ entry_sort_order }}" id="input-sort-order" class="form-control" />*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="tab-pane" id="tab-links">*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-manufacturer"><span data-toggle="tooltip" title="{{ help_manufacturer }}">{{ entry_manufacturer }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="manufacturer" value="{{ manufacturer }}" placeholder="{{ entry_manufacturer }}" id="input-manufacturer" class="form-control" />*/
/*                   <input type="hidden" name="manufacturer_id" value="{{ manufacturer_id }}" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="{{ help_category }}">{{ entry_category }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="category" value="" placeholder="{{ entry_category }}" id="input-category" class="form-control" />*/
/*                   <div id="information-category" class="well well-sm" style="height: 150px; overflow: auto;"> {% for information_category in information_categories %}*/
/*                     {% if main_category and main_category == information_category.category_id %}*/
/*                     <div id="information-category{{ information_category.category_id }}" class="text-success"><i class="fa fa-minus-circle"></i> <i class="fa fa-dot-circle-o" data-toggle="tooltip" title="{{ help_main_category }}"></i> {{ information_category.name }}*/
/*                       <input type="hidden" name="information_category[]" value="{{ information_category.category_id }}" />*/
/*                       <input type="hidden" name="main_category" value="{{ information_category.category_id }}" />*/
/*                     </div>*/
/*                     {% else %}*/
/*                     <div id="information-category{{ information_category.category_id }}"><i class="fa fa-minus-circle"></i> <i class="fa fa-circle-o" data-toggle="tooltip" title="{{ help_main_category_add }}"></i> {{ information_category.name }}*/
/*                       <input type="hidden" name="information_category[]" value="{{ information_category.category_id }}" />*/
/*                     </div>*/
/*                     {% endif %}*/
/*                     {% endfor %}</div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="{{ help_filter }}">{{ entry_filter }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="filter" value="" placeholder="{{ entry_filter }}" id="input-filter" class="form-control" />*/
/*                   <div id="information-filter" class="well well-sm" style="height: 150px; overflow: auto;"> {% for information_filter in information_filters %}*/
/*                     <div id="information-filter{{ information_filter.filter_id }}"><i class="fa fa-minus-circle"></i> {{ information_filter.name }}*/
/*                       <input type="hidden" name="information_filter[]" value="{{ information_filter.filter_id }}" />*/
/*                     </div>*/
/*                     {% endfor %}</div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-related-product"><span data-toggle="tooltip" title="{{ help_related }}">{{ entry_related_product }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="related_product" value="" placeholder="{{ entry_related_product }}" id="input-related-product" class="form-control" />*/
/*                   <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;"> {% for product_related in product_relateds %}*/
/*                     <div id="product-related{{ product_related.product_id }}"><i class="fa fa-minus-circle"></i> */
/*                       {% if product_related.route and product_related.route > 0 %}*/
/*                       <i class="fa fa-long-arrow-right"></i> */
/*                       {% elseif product_related.route and product_related.route < 0 %}*/
/*                       <i class="fa fa-long-arrow-left"></i> */
/*                       {% else %}*/
/*                       <i class="fa fa-exchange"></i> */
/*                       {% endif %}*/
/*                       {{ product_related.name }}*/
/*                       <input type="hidden" name="product_related[{{ product_related.product_id }}][product_id]" value="{{ product_related.product_id }}" />*/
/*                       <input type="hidden" name="product_related[{{ product_related.product_id }}][route]" value="{{ product_related.route }}" />*/
/*                     </div>*/
/*                     {% endfor %}</div>*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-related-information"><span data-toggle="tooltip" title="{{ help_related }}">{{ entry_related_information }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" name="related_information" value="" placeholder="{{ entry_related_information }}" id="input-related-information" class="form-control" />*/
/*                   <div id="information-related" class="well well-sm" style="height: 150px; overflow: auto;"> {% for information_related in information_relateds %}*/
/*                     <div id="information-related{{ information_related.information_id }}"><i class="fa fa-minus-circle"></i> */
/*                       {% if information_related.route and information_related.route > 0 %}*/
/*                       <i class="fa fa-long-arrow-right"></i> */
/*                       {% elseif information_related.route and information_related.route < 0 %}*/
/*                       <i class="fa fa-long-arrow-left"></i> */
/*                       {% else %}*/
/*                       <i class="fa fa-exchange"></i> */
/*                       {% endif %}*/
/*                       {{ information_related.title }}*/
/*                       <input type="hidden" name="information_related[{{ information_related.information_id }}][information_id]" value="{{ information_related.information_id }}" />*/
/*                       <input type="hidden" name="information_related[{{ information_related.information_id }}][route]" value="{{ information_related.route }}" />*/
/*                     </div>*/
/*                     {% endfor %}</div>*/
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-attribute">*/
/*               <div class="table-responsive">*/
/*                 <table id="attribute" class="table table-striped table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td class="text-left">{{ entry_attribute }}</td>*/
/*                       <td class="text-left">{{ entry_text }}</td>*/
/*                       <td></td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                     {% set attribute_row = 0 %}*/
/*                     {% for information_attribute in information_attributes %}*/
/*                     <tr id="attribute-row{{ attribute_row }}">*/
/*                       <td class="text-left" style="width: 40%;"><input type="text" name="information_attribute[{{ attribute_row }}][name]" value="{{ information_attribute.name }}" placeholder="{{ entry_attribute }}" class="form-control" />*/
/*                       <input type="hidden" name="information_attribute[{{ attribute_row }}][attribute_id]" value="{{ information_attribute.attribute_id }}" /></td>*/
/*                       <td class="text-left">{% for language in languages %}*/
/*                         <div class="input-group"><span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                           <textarea name="information_attribute[{{ attribute_row }}][information_attribute_description][{{ language.language_id }}][text]" rows="5" placeholder="{{ entry_text }}" class="form-control">{{ information_attribute.information_attribute_description[language.language_id] ? information_attribute.information_attribute_description[language.language_id].text }}</textarea>*/
/*                         </div>*/
/*                         {% endfor %}</td>*/
/*                       <td class="text-right"><button type="button" onclick="$('#attribute-row{{ attribute_row }}').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>*/
/*                     </tr>*/
/*                     {% set attribute_row = attribute_row + 1 %}*/
/*                     {% endfor %}*/
/*                   </tbody>*/
/*                   <tfoot>*/
/*                     <tr>*/
/*                       <td colspan="2"></td>*/
/*                       <td class="text-right"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="{{ button_attribute_add }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>*/
/*                     </tr>*/
/*                   </tfoot>*/
/*                 </table>*/
/*               </div>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-image">*/
/*               <div class="table-responsive">*/
/*                 <table class="table table-striped table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td class="text-left">{{ entry_image }}</td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                     <tr>*/
/*                       <td class="text-left"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="{{ thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                         <input type="hidden" name="image" value="{{ image }}" id="input-image" /></td>*/
/*                     </tr>*/
/*                   </tbody>*/
/*                 </table>*/
/*               </div>*/
/*               <div class="table-responsive">*/
/*                 <table id="images" class="table table-striped table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td class="text-left">{{ entry_additional_image }}</td>*/
/*                       <td class="text-right">{{ entry_sort_order }}</td>*/
/*                       <td></td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                     {% set image_row = 0 %}*/
/*                     {% for information_image in information_images %}*/
/*                     <tr id="image-row{{ image_row }}">*/
/*                       <td class="text-left"><a href="" id="thumb-image{{ image_row }}" data-toggle="image" class="img-thumbnail"><img src="{{ information_image.thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                         <input type="hidden" name="information_image[{{ image_row }}][image]" value="{{ information_image.image }}" id="input-image{{ image_row }}" /></td>*/
/*                       <td class="text-right"><input type="text" name="information_image[{{ image_row }}][sort_order]" value="{{ information_image.sort_order }}" placeholder="{{ entry_sort_order }}" class="form-control" /></td>*/
/*                       <td class="text-left"><button type="button" onclick="$('#image-row{{ image_row }}').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>*/
/*                     </tr>*/
/*                     {% set image_row = image_row + 1 %}*/
/*                     {% endfor %}*/
/*                   </tbody>*/
/*                   <tfoot>*/
/*                     <tr>*/
/*                       <td colspan="2"></td>*/
/*                       <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="{{ button_image_add }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>*/
/*                     </tr>*/
/*                   </tfoot>*/
/*                 </table>*/
/*               </div>*/
/*             </div>*/
/*         */
/*             <div class="tab-pane" id="tab-seo">*/
/*               <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_keyword }}</div>*/
/*               <div class="table-responsive">*/
/*                 <table class="table table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td class="text-left">{{ entry_store }}</td>*/
/*                       <td class="text-left">{{ entry_keyword }}</td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                   {% for store in stores %}*/
/*                   <tr>*/
/*                     <td class="text-left">{{ store.name }}</td>*/
/*                     <td class="text-left">{% for language in languages %}*/
/*                       <div class="input-group"><span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                         <input type="text" name="information_seo_url[{{ store.store_id }}][{{ language.language_id }}]" value="{% if information_seo_url[store.store_id][language.language_id] %}{{ information_seo_url[store.store_id][language.language_id] }}{% endif %}" placeholder="{{ entry_keyword }}" class="form-control" />*/
/*                       </div>*/
/*                       {% if error_keyword[store.store_id][language.language_id] %}*/
/*                       <div class="text-danger">{{ error_keyword[store.store_id][language.language_id] }}</div>*/
/*                       {% endif %} */
/*                       {% endfor %}</td>*/
/*                   </tr>*/
/*                   {% endfor %}*/
/*                   </tbody>*/
/*                 </table>*/
/*               </div>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-design">*/
/*               <div class="table-responsive">*/
/*                 <table class="table table-bordered table-hover">*/
/*                   <thead>*/
/*                     <tr>*/
/*                       <td class="text-left">{{ entry_store }}</td>*/
/*                       <td class="text-left">{{ entry_layout }}</td>*/
/*                     </tr>*/
/*                   </thead>*/
/*                   <tbody>*/
/*                   */
/*                   {% for store in stores %}*/
/*                   <tr>*/
/*                     <td class="text-left">{{ store.name }}</td>*/
/*                     <td class="text-left"><select name="information_layout[{{ store.store_id }}]" class="form-control">*/
/*                         <option value=""></option>*/
/*                         {% for layout in layouts %}*/
/*                         {% if information_layout[store.store_id] and information_layout[store.store_id] == layout.layout_id %}*/
/*                         <option value="{{ layout.layout_id }}" selected="selected">{{ layout.name }}</option>*/
/*                         {% else %}*/
/*                         <option value="{{ layout.layout_id }}">{{ layout.name }}</option>*/
/*                         {% endif %}*/
/*                         {% endfor %}*/
/*                       </select></td>*/
/*                   </tr>*/
/*                   {% endfor %}*/
/*                     </tbody>*/
/*                   */
/*                 </table>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <link href="view/javascript/codemirror/lib/codemirror.css" rel="stylesheet" />*/
/*   <link href="view/javascript/codemirror/theme/monokai.css" rel="stylesheet" />*/
/*   <script type="text/javascript" src="view/javascript/codemirror/lib/codemirror.js"></script> */
/*   <script type="text/javascript" src="view/javascript/codemirror/lib/xml.js"></script> */
/*   <script type="text/javascript" src="view/javascript/codemirror/lib/formatting.js"></script> */
/*   <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/*   <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/*   <script type="text/javascript" src="view/javascript/summernote/summernote-image-attributes.js"></script> */
/*   <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> */
/* */
/*   <script type="text/javascript"><!--*/
/* // Manufacturer*/
/* $('input[name=\'manufacturer\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/manufacturer/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				json.unshift({*/
/* 					manufacturer_id: 0,*/
/* 					name: '{{ text_none }}'*/
/* 				});*/
/* */
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['manufacturer_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'manufacturer\']').val(item['label']);*/
/* 		$('input[name=\'manufacturer_id\']').val(item['value']);*/
/* 	}*/
/* });*/
/* */
/* // Category*/
/* $('input[name=\'category\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/category/autocomplete&user_token={{ user_token }}&filter_information=1&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['category_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'category\']').val('');*/
/* */
/* 		$('#information-category' + item['value']).remove();*/
/* */
/* 		$('#information-category').append('<div id="information-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> <i class="fa fa-circle-o" data-toggle="tooltip" title="{{ help_main_category_add }}"></i> ' + item['label'] + '<input type="hidden" name="information_category[]" value="' + item['value'] + '" /></div>');*/
/* 	}*/
/* });*/
/* */
/* $('#information-category').delegate('.fa-minus-circle', 'click', function() {*/
/* 	$(this).parent().remove();*/
/* });*/
/* */
/* $('#information-category').delegate('.fa-circle-o', 'click', function() {*/
/* 	$('#information-category div').children('.fa-dot-circle-o').toggleClass('fa-circle-o fa-dot-circle-o');*/
/*     $('#information-category div').removeClass('text-success');*/
/*     $(this).toggleClass('fa-circle-o fa-dot-circle-o');*/
/*     $(this).parent().addClass('text-success');*/
/*     $('input[name=\'main_category\']').remove();*/
/*     $(this).parent().append('<input type="hidden" name="main_category" value="' + $(this).parent().find('input[name=\'information_category[]\']').val() + '" />');*/
/* 	$('#information-category div').children('.fa-circle-o').attr({'title':'{{ help_main_category_add }}', 'data-original-title':'{{ help_main_category_add }}'});*/
/*     $(this).attr({'title':'{{ help_main_category }}', 'data-original-title':'{{ help_main_category }}'});*/
/* });*/
/* $('#information-category').delegate('.fa-dot-circle-o', 'click', function() {*/
/*     $(this).toggleClass('fa-circle-o fa-dot-circle-o');*/
/*     $('input[name=\'main_category\']').remove();*/
/*     $(this).parent().removeClass('text-success');*/
/*     $(this).attr({'title':'{{ help_main_category_add }}', 'data-original-title':'{{ help_main_category_add }}'});*/
/* });*/
/* */
/* // Filter*/
/* $('input[name=\'filter\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/filter/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['filter_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter\']').val('');*/
/* */
/* 		$('#information-filter' + item['value']).remove();*/
/* */
/* 		$('#information-filter').append('<div id="information-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="information_filter[]" value="' + item['value'] + '" /></div>');*/
/* 	}*/
/* });*/
/* */
/* $('#information-filter').delegate('.fa-minus-circle', 'click', function() {*/
/* 	$(this).parent().remove();*/
/* });*/
/* */
/* // Related Information*/
/* $('input[name=\'related_information\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/information/autocomplete&user_token={{ user_token }}{% if information_id %}&information_id={{ information_id }}{% endif %}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['title'],*/
/* 						value: item['information_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'related_information\']').val('');*/
/* */
/* 		$('#information-related' + item['value']).remove();*/
/* */
/* 		$('#information-related').append('<div id="information-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> <i class="fa fa-exchange"></i> ' + item['label'] + '<input type="hidden" name="information_related[' + item['value'] + '][information_id]" value="' + item['value'] + '" /><input type="hidden" name="information_related[' + item['value'] + '][route]" value="0" /></div>');*/
/* 	}*/
/* });*/
/* */
/* $('#information-related').delegate('.fa-minus-circle', 'click', function() {*/
/* 	$(this).parent().remove();*/
/* });*/
/* */
/* $('#information-related').delegate('.fa-exchange', 'click', function() {*/
/*     $(this).toggleClass('fa-exchange fa-long-arrow-right');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('1');*/
/* });*/
/* $('#information-related').delegate('.fa-long-arrow-right', 'click', function() {*/
/*     $(this).toggleClass('fa-long-arrow-right fa-long-arrow-left');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('-1');*/
/* });*/
/* $('#information-related').delegate('.fa-long-arrow-left', 'click', function() {*/
/*     $(this).toggleClass('fa-long-arrow-left fa-exchange');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('0');*/
/* });*/
/* */
/* // Related Product*/
/* $('input[name=\'related_product\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'related_product\']').val('');*/
/* */
/* 		$('#product-related' + item['value']).remove();*/
/* */
/* 		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> <i class="fa fa-exchange"></i> ' + item['label'] + '<input type="hidden" name="product_related[' + item['value'] + '][product_id]" value="' + item['value'] + '" /><input type="hidden" name="product_related[' + item['value'] + '][route]" value="0" /></div>');*/
/* 	}*/
/* });*/
/* */
/* $('#product-related').delegate('.fa-minus-circle', 'click', function() {*/
/* 	$(this).parent().remove();*/
/* });*/
/* */
/* $('#product-related').delegate('.fa-exchange', 'click', function() {*/
/*     $(this).toggleClass('fa-exchange fa-long-arrow-right');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('1');*/
/* });*/
/* $('#product-related').delegate('.fa-long-arrow-right', 'click', function() {*/
/*     $(this).toggleClass('fa-long-arrow-right fa-long-arrow-left');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('-1');*/
/* });*/
/* $('#product-related').delegate('.fa-long-arrow-left', 'click', function() {*/
/*     $(this).toggleClass('fa-long-arrow-left fa-exchange');*/
/* 	$(this).parent().find('input[name$=\'[route]\']').val('0');*/
/* });*/
/* //--></script> */
/*   <script type="text/javascript"><!--*/
/* var attribute_row = {{ attribute_row }};*/
/* */
/* function addAttribute() {*/
/*     html  = '<tr id="attribute-row' + attribute_row + '">';*/
/* 	html += '  <td class="text-left" style="width: 20%;"><input type="text" name="information_attribute[' + attribute_row + '][name]" value="" placeholder="{{ entry_attribute }}" class="form-control" /><input type="hidden" name="information_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';*/
/* 	html += '  <td class="text-left">';*/
/* 	{% for language in languages %}*/
/* 	html += '<div class="input-group"><span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span><textarea name="information_attribute[' + attribute_row + '][information_attribute_description][{{ language.language_id }}][text]" rows="5" placeholder="{{ entry_text }}" class="form-control"></textarea></div>';*/
/*     {% endfor %}*/
/* 	html += '  </td>';*/
/* 	html += '  <td class="text-right"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';*/
/*     html += '</tr>';*/
/* */
/* 	$('#attribute tbody').append(html);*/
/* */
/* 	attributeautocomplete(attribute_row);*/
/* */
/* 	attribute_row++;*/
/* }*/
/* */
/* function attributeautocomplete(attribute_row) {*/
/* 	$('input[name=\'information_attribute[' + attribute_row + '][name]\']').autocomplete({*/
/* 		'source': function(request, response) {*/
/* 			$.ajax({*/
/* 				url: 'index.php?route=catalog/attribute/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 				dataType: 'json',*/
/* 				success: function(json) {*/
/* 					response($.map(json, function(item) {*/
/* 						return {*/
/* 							category: item.attribute_group,*/
/* 							label: item.name,*/
/* 							value: item.attribute_id*/
/* 						}*/
/* 					}));*/
/* 				}*/
/* 			});*/
/* 		},*/
/* 		'select': function(item) {*/
/* 			$('input[name=\'information_attribute[' + attribute_row + '][name]\']').val(item['label']);*/
/* 			$('input[name=\'information_attribute[' + attribute_row + '][attribute_id]\']').val(item['value']);*/
/* 		}*/
/* 	});*/
/* }*/
/* */
/* $('#attribute tbody tr').each(function(index, element) {*/
/* 	attributeautocomplete(index);*/
/* });*/
/* //--></script> */
/*   <script type="text/javascript"><!--*/
/* var image_row = {{ image_row }};*/
/* */
/* function addImage() {*/
/* 	html  = '<tr id="image-row' + image_row + '">';*/
/* 	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="{{ placeholder }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a><input type="hidden" name="information_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';*/
/* 	html += '  <td class="text-right"><input type="text" name="information_image[' + image_row + '][sort_order]" value="" placeholder="{{ entry_sort_order }}" class="form-control" /></td>';*/
/* 	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';*/
/* 	html += '</tr>';*/
/* */
/* 	$('#images tbody').append(html);*/
/* */
/* 	image_row++;*/
/* }*/
/* //--></script> */
/*   <script type="text/javascript"><!--*/
/* $('.date').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickTime: false*/
/* });*/
/* */
/* $('.time').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickDate: false*/
/* });*/
/* */
/* $('.datetime').datetimepicker({*/
/* 	language: '{{ datepicker }}',*/
/* 	pickDate: true,*/
/* 	pickTime: true*/
/* });*/
/* //--></script> */
/*         */
/*   <script type="text/javascript"><!--*/
/* $('#language a:first').tab('show');*/
/* //--></script></div>*/
/* */
/*   <style type="text/css"><!--*/
/* #information-category .fa-circle-o {*/
/*     display: none;*/
/* }*/
/* #information-category div:hover .fa-circle-o {*/
/*     display: inline;*/
/* }*/
/* --></style>*/
/*   <script type="text/javascript"><!--*/
/* $('.summernote').on('click', function () {*/
/* 	if ($(this).hasClass('active')) {*/
/* 		$('#input-short-description' + $(this).data('language')).summernote('destroy');*/
/* 	} else {*/
/* 		$('#input-short-description' + $(this).data('language')).summernote({*/
/* 			focus: true,*/
/* 			lang: '{{ summernote }}'*/
/* 		});*/
/* 	}*/
/* })*/
/* */
/* $(document).ready(function() {*/
/* 	$('[name*=\'[short_description]\']').each(function() {*/
/* 		if ($(this).val()) {*/
/*             $(this).parent().prev().children('.summernote').trigger('click');*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script>*/
/*         */
/* {{ footer }} */
/* */
